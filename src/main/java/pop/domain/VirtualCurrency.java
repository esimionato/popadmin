package pop.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A VirtualCurrency.
 */
@Entity
@Table(name = "virtual_currency")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class VirtualCurrency implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "currency_icon_url")
    private String currencyIconUrl;

    @NotNull
    @Size(max = 2)
    @Pattern(regexp = "^[_'.@A-Za-z0-9-]*$")
    @Column(name = "currency_code", length = 2, nullable = false)
    private String currencyCode;

    @NotNull
    @Size(max = 80)
    @Pattern(regexp = "^[_'.@A-Za-z0-9-]*$")
    @Column(name = "currency_name", length = 80, nullable = false)
    private String currencyName;

    @Column(name = "initial_deposit")
    private Integer initialDeposit;

    @Column(name = "recharge_rate")
    private Integer rechargeRate;

    @Column(name = "max_limit")
    private Integer maxLimit;

    @Column(name = "currency_ratio")
    private Integer currencyRatio;

    @Column(name = "currency_base")
    private Boolean currencyBase;

    @ManyToOne
    private Game game;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCurrencyIconUrl() {
        return currencyIconUrl;
    }

    public VirtualCurrency currencyIconUrl(String currencyIconUrl) {
        this.currencyIconUrl = currencyIconUrl;
        return this;
    }

    public void setCurrencyIconUrl(String currencyIconUrl) {
        this.currencyIconUrl = currencyIconUrl;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public VirtualCurrency currencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public VirtualCurrency currencyName(String currencyName) {
        this.currencyName = currencyName;
        return this;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public Integer getInitialDeposit() {
        return initialDeposit;
    }

    public VirtualCurrency initialDeposit(Integer initialDeposit) {
        this.initialDeposit = initialDeposit;
        return this;
    }

    public void setInitialDeposit(Integer initialDeposit) {
        this.initialDeposit = initialDeposit;
    }

    public Integer getRechargeRate() {
        return rechargeRate;
    }

    public VirtualCurrency rechargeRate(Integer rechargeRate) {
        this.rechargeRate = rechargeRate;
        return this;
    }

    public void setRechargeRate(Integer rechargeRate) {
        this.rechargeRate = rechargeRate;
    }

    public Integer getMaxLimit() {
        return maxLimit;
    }

    public VirtualCurrency maxLimit(Integer maxLimit) {
        this.maxLimit = maxLimit;
        return this;
    }

    public void setMaxLimit(Integer maxLimit) {
        this.maxLimit = maxLimit;
    }

    public Integer getCurrencyRatio() {
        return currencyRatio;
    }

    public VirtualCurrency currencyRatio(Integer currencyRatio) {
        this.currencyRatio = currencyRatio;
        return this;
    }

    public void setCurrencyRatio(Integer currencyRatio) {
        this.currencyRatio = currencyRatio;
    }

    public Boolean isCurrencyBase() {
        return currencyBase;
    }

    public VirtualCurrency currencyBase(Boolean currencyBase) {
        this.currencyBase = currencyBase;
        return this;
    }

    public void setCurrencyBase(Boolean currencyBase) {
        this.currencyBase = currencyBase;
    }

    public Game getGame() {
        return game;
    }

    public VirtualCurrency game(Game game) {
        this.game = game;
        return this;
    }

    public void setGame(Game game) {
        this.game = game;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VirtualCurrency virtualCurrency = (VirtualCurrency) o;
        if (virtualCurrency.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), virtualCurrency.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "VirtualCurrency{" +
            "id=" + getId() +
            ", currencyIconUrl='" + getCurrencyIconUrl() + "'" +
            ", currencyCode='" + getCurrencyCode() + "'" +
            ", currencyName='" + getCurrencyName() + "'" +
            ", initialDeposit='" + getInitialDeposit() + "'" +
            ", rechargeRate='" + getRechargeRate() + "'" +
            ", maxLimit='" + getMaxLimit() + "'" +
            ", currencyRatio='" + getCurrencyRatio() + "'" +
            ", currencyBase='" + isCurrencyBase() + "'" +
            "}";
    }
}
