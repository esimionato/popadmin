package pop.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A PlayerVcurrencyBalance.
 */
@Entity
@Table(name = "player_vcurrency_balance")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PlayerVcurrencyBalance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "amount")
    private Integer amount;

    @ManyToOne
    private Player player;

    @ManyToOne
    private VirtualCurrency virtual_currency;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAmount() {
        return amount;
    }

    public PlayerVcurrencyBalance amount(Integer amount) {
        this.amount = amount;
        return this;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Player getPlayer() {
        return player;
    }

    public PlayerVcurrencyBalance player(Player player) {
        this.player = player;
        return this;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public VirtualCurrency getVirtual_currency() {
        return virtual_currency;
    }

    public PlayerVcurrencyBalance virtual_currency(VirtualCurrency VirtualCurrency) {
        this.virtual_currency = VirtualCurrency;
        return this;
    }

    public void setVirtual_currency(VirtualCurrency VirtualCurrency) {
        this.virtual_currency = VirtualCurrency;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PlayerVcurrencyBalance playerVcurrencyBalance = (PlayerVcurrencyBalance) o;
        if (playerVcurrencyBalance.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), playerVcurrencyBalance.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PlayerVcurrencyBalance{" +
            "id=" + getId() +
            ", amount='" + getAmount() + "'" +
            "}";
    }
}
