package pop.repository;

import pop.domain.VirtualCurrency;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the VirtualCurrency entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VirtualCurrencyRepository extends JpaRepository<VirtualCurrency, Long>, JpaSpecificationExecutor<VirtualCurrency> {

}
