package pop.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import pop.domain.PlayerVcurrencyBalance;
import pop.domain.*; // for static metamodels
import pop.repository.PlayerVcurrencyBalanceRepository;
import pop.service.dto.PlayerVcurrencyBalanceCriteria;

import pop.service.dto.PlayerVcurrencyBalanceDTO;
import pop.service.mapper.PlayerVcurrencyBalanceMapper;

/**
 * Service for executing complex queries for PlayerVcurrencyBalance entities in the database.
 * The main input is a {@link PlayerVcurrencyBalanceCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link PlayerVcurrencyBalanceDTO} or a {@link Page} of {%link PlayerVcurrencyBalanceDTO} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class PlayerVcurrencyBalanceQueryService extends QueryService<PlayerVcurrencyBalance> {

    private final Logger log = LoggerFactory.getLogger(PlayerVcurrencyBalanceQueryService.class);


    private final PlayerVcurrencyBalanceRepository playerVcurrencyBalanceRepository;

    private final PlayerVcurrencyBalanceMapper playerVcurrencyBalanceMapper;

    public PlayerVcurrencyBalanceQueryService(PlayerVcurrencyBalanceRepository playerVcurrencyBalanceRepository, PlayerVcurrencyBalanceMapper playerVcurrencyBalanceMapper) {
        this.playerVcurrencyBalanceRepository = playerVcurrencyBalanceRepository;
        this.playerVcurrencyBalanceMapper = playerVcurrencyBalanceMapper;
    }

    /**
     * Return a {@link List} of {%link PlayerVcurrencyBalanceDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PlayerVcurrencyBalanceDTO> findByCriteria(PlayerVcurrencyBalanceCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<PlayerVcurrencyBalance> specification = createSpecification(criteria);
        return playerVcurrencyBalanceMapper.toDto(playerVcurrencyBalanceRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {%link PlayerVcurrencyBalanceDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PlayerVcurrencyBalanceDTO> findByCriteria(PlayerVcurrencyBalanceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<PlayerVcurrencyBalance> specification = createSpecification(criteria);
        final Page<PlayerVcurrencyBalance> result = playerVcurrencyBalanceRepository.findAll(specification, page);
        return result.map(playerVcurrencyBalanceMapper::toDto);
    }

    /**
     * Function to convert PlayerVcurrencyBalanceCriteria to a {@link Specifications}
     */
    private Specifications<PlayerVcurrencyBalance> createSpecification(PlayerVcurrencyBalanceCriteria criteria) {
        Specifications<PlayerVcurrencyBalance> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), PlayerVcurrencyBalance_.id));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), PlayerVcurrencyBalance_.amount));
            }
            if (criteria.getPlayerId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getPlayerId(), PlayerVcurrencyBalance_.player, Player_.id));
            }
            if (criteria.getVirtual_currencyId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getVirtual_currencyId(), PlayerVcurrencyBalance_.virtual_currency, VirtualCurrency_.id));
            }
        }
        return specification;
    }

}
