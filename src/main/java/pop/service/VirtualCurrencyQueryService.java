package pop.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import pop.domain.VirtualCurrency;
import pop.domain.*; // for static metamodels
import pop.repository.VirtualCurrencyRepository;
import pop.service.dto.VirtualCurrencyCriteria;

import pop.service.dto.VirtualCurrencyDTO;
import pop.service.mapper.VirtualCurrencyMapper;

/**
 * Service for executing complex queries for VirtualCurrency entities in the database.
 * The main input is a {@link VirtualCurrencyCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {%link VirtualCurrencyDTO} or a {@link Page} of {%link VirtualCurrencyDTO} which fulfills the criterias
 */
@Service
@Transactional(readOnly = true)
public class VirtualCurrencyQueryService extends QueryService<VirtualCurrency> {

    private final Logger log = LoggerFactory.getLogger(VirtualCurrencyQueryService.class);


    private final VirtualCurrencyRepository virtualCurrencyRepository;

    private final VirtualCurrencyMapper virtualCurrencyMapper;

    public VirtualCurrencyQueryService(VirtualCurrencyRepository virtualCurrencyRepository, VirtualCurrencyMapper virtualCurrencyMapper) {
        this.virtualCurrencyRepository = virtualCurrencyRepository;
        this.virtualCurrencyMapper = virtualCurrencyMapper;
    }

    /**
     * Return a {@link List} of {%link VirtualCurrencyDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<VirtualCurrencyDTO> findByCriteria(VirtualCurrencyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<VirtualCurrency> specification = createSpecification(criteria);
        return virtualCurrencyMapper.toDto(virtualCurrencyRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {%link VirtualCurrencyDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<VirtualCurrencyDTO> findByCriteria(VirtualCurrencyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<VirtualCurrency> specification = createSpecification(criteria);
        final Page<VirtualCurrency> result = virtualCurrencyRepository.findAll(specification, page);
        return result.map(virtualCurrencyMapper::toDto);
    }

    /**
     * Function to convert VirtualCurrencyCriteria to a {@link Specifications}
     */
    private Specifications<VirtualCurrency> createSpecification(VirtualCurrencyCriteria criteria) {
        Specifications<VirtualCurrency> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), VirtualCurrency_.id));
            }
            if (criteria.getCurrencyIconUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCurrencyIconUrl(), VirtualCurrency_.currencyIconUrl));
            }
            if (criteria.getCurrencyCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCurrencyCode(), VirtualCurrency_.currencyCode));
            }
            if (criteria.getCurrencyName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCurrencyName(), VirtualCurrency_.currencyName));
            }
            if (criteria.getInitialDeposit() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getInitialDeposit(), VirtualCurrency_.initialDeposit));
            }
            if (criteria.getRechargeRate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getRechargeRate(), VirtualCurrency_.rechargeRate));
            }
            if (criteria.getMaxLimit() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaxLimit(), VirtualCurrency_.maxLimit));
            }
            if (criteria.getCurrencyRatio() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCurrencyRatio(), VirtualCurrency_.currencyRatio));
            }
            if (criteria.getCurrencyBase() != null) {
                specification = specification.and(buildSpecification(criteria.getCurrencyBase(), VirtualCurrency_.currencyBase));
            }
            if (criteria.getGameId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getGameId(), VirtualCurrency_.game, Game_.id));
            }
        }
        return specification;
    }

}
