package pop.service.impl;

import pop.service.PlayerVcurrencyBalanceService;
import pop.domain.PlayerVcurrencyBalance;
import pop.repository.PlayerVcurrencyBalanceRepository;
import pop.service.dto.PlayerVcurrencyBalanceDTO;
import pop.service.mapper.PlayerVcurrencyBalanceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing PlayerVcurrencyBalance.
 */
@Service
@Transactional
public class PlayerVcurrencyBalanceServiceImpl implements PlayerVcurrencyBalanceService{

    private final Logger log = LoggerFactory.getLogger(PlayerVcurrencyBalanceServiceImpl.class);

    private final PlayerVcurrencyBalanceRepository playerVcurrencyBalanceRepository;

    private final PlayerVcurrencyBalanceMapper playerVcurrencyBalanceMapper;

    public PlayerVcurrencyBalanceServiceImpl(PlayerVcurrencyBalanceRepository playerVcurrencyBalanceRepository, PlayerVcurrencyBalanceMapper playerVcurrencyBalanceMapper) {
        this.playerVcurrencyBalanceRepository = playerVcurrencyBalanceRepository;
        this.playerVcurrencyBalanceMapper = playerVcurrencyBalanceMapper;
    }

    /**
     * Save a playerVcurrencyBalance.
     *
     * @param playerVcurrencyBalanceDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PlayerVcurrencyBalanceDTO save(PlayerVcurrencyBalanceDTO playerVcurrencyBalanceDTO) {
        log.debug("Request to save PlayerVcurrencyBalance : {}", playerVcurrencyBalanceDTO);
        PlayerVcurrencyBalance playerVcurrencyBalance = playerVcurrencyBalanceMapper.toEntity(playerVcurrencyBalanceDTO);
        playerVcurrencyBalance = playerVcurrencyBalanceRepository.save(playerVcurrencyBalance);
        return playerVcurrencyBalanceMapper.toDto(playerVcurrencyBalance);
    }

    /**
     *  Get all the playerVcurrencyBalances.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PlayerVcurrencyBalanceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PlayerVcurrencyBalances");
        return playerVcurrencyBalanceRepository.findAll(pageable)
            .map(playerVcurrencyBalanceMapper::toDto);
    }

    /**
     *  Get one playerVcurrencyBalance by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PlayerVcurrencyBalanceDTO findOne(Long id) {
        log.debug("Request to get PlayerVcurrencyBalance : {}", id);
        PlayerVcurrencyBalance playerVcurrencyBalance = playerVcurrencyBalanceRepository.findOne(id);
        return playerVcurrencyBalanceMapper.toDto(playerVcurrencyBalance);
    }

    /**
     *  Delete the  playerVcurrencyBalance by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete PlayerVcurrencyBalance : {}", id);
        playerVcurrencyBalanceRepository.delete(id);
    }
}
