package pop.service.impl;

import pop.service.VirtualCurrencyService;
import pop.domain.VirtualCurrency;
import pop.repository.VirtualCurrencyRepository;
import pop.service.dto.VirtualCurrencyDTO;
import pop.service.mapper.VirtualCurrencyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Service Implementation for managing VirtualCurrency.
 */
@Service
@Transactional
public class VirtualCurrencyServiceImpl implements VirtualCurrencyService{

    private final Logger log = LoggerFactory.getLogger(VirtualCurrencyServiceImpl.class);

    private final VirtualCurrencyRepository virtualCurrencyRepository;

    private final VirtualCurrencyMapper virtualCurrencyMapper;

    public VirtualCurrencyServiceImpl(VirtualCurrencyRepository virtualCurrencyRepository, VirtualCurrencyMapper virtualCurrencyMapper) {
        this.virtualCurrencyRepository = virtualCurrencyRepository;
        this.virtualCurrencyMapper = virtualCurrencyMapper;
    }

    /**
     * Save a virtualCurrency.
     *
     * @param virtualCurrencyDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public VirtualCurrencyDTO save(VirtualCurrencyDTO virtualCurrencyDTO) {
        log.debug("Request to save VirtualCurrency : {}", virtualCurrencyDTO);
        VirtualCurrency virtualCurrency = virtualCurrencyMapper.toEntity(virtualCurrencyDTO);
        virtualCurrency = virtualCurrencyRepository.save(virtualCurrency);
        return virtualCurrencyMapper.toDto(virtualCurrency);
    }

    /**
     *  Get all the virtualCurrencies.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<VirtualCurrencyDTO> findAll(Pageable pageable) {
        log.debug("Request to get all VirtualCurrencies");
        return virtualCurrencyRepository.findAll(pageable)
            .map(virtualCurrencyMapper::toDto);
    }

    /**
     *  Get one virtualCurrency by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public VirtualCurrencyDTO findOne(Long id) {
        log.debug("Request to get VirtualCurrency : {}", id);
        VirtualCurrency virtualCurrency = virtualCurrencyRepository.findOne(id);
        return virtualCurrencyMapper.toDto(virtualCurrency);
    }

    /**
     *  Delete the  virtualCurrency by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete VirtualCurrency : {}", id);
        virtualCurrencyRepository.delete(id);
    }
}
