package pop.service;

import pop.service.dto.VirtualCurrencyDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing VirtualCurrency.
 */
public interface VirtualCurrencyService {

    /**
     * Save a virtualCurrency.
     *
     * @param virtualCurrencyDTO the entity to save
     * @return the persisted entity
     */
    VirtualCurrencyDTO save(VirtualCurrencyDTO virtualCurrencyDTO);

    /**
     *  Get all the virtualCurrencies.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<VirtualCurrencyDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" virtualCurrency.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    VirtualCurrencyDTO findOne(Long id);

    /**
     *  Delete the "id" virtualCurrency.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);
}
