package pop.web.rest;

import com.codahale.metrics.annotation.Timed;
import pop.service.VirtualCurrencyService;
import pop.web.rest.errors.BadRequestAlertException;
import pop.web.rest.util.HeaderUtil;
import pop.web.rest.util.PaginationUtil;
import pop.service.dto.VirtualCurrencyDTO;
import pop.service.dto.VirtualCurrencyCriteria;
import pop.service.VirtualCurrencyQueryService;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing VirtualCurrency.
 */
@RestController
@RequestMapping("/api")
public class VirtualCurrencyResource {

    private final Logger log = LoggerFactory.getLogger(VirtualCurrencyResource.class);

    private static final String ENTITY_NAME = "virtualCurrency";

    private final VirtualCurrencyService virtualCurrencyService;

    private final VirtualCurrencyQueryService virtualCurrencyQueryService;

    public VirtualCurrencyResource(VirtualCurrencyService virtualCurrencyService, VirtualCurrencyQueryService virtualCurrencyQueryService) {
        this.virtualCurrencyService = virtualCurrencyService;
        this.virtualCurrencyQueryService = virtualCurrencyQueryService;
    }

    /**
     * POST  /virtual-currencies : Create a new virtualCurrency.
     *
     * @param virtualCurrencyDTO the virtualCurrencyDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new virtualCurrencyDTO, or with status 400 (Bad Request) if the virtualCurrency has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/virtual-currencies")
    @Timed
    public ResponseEntity<VirtualCurrencyDTO> createVirtualCurrency(@Valid @RequestBody VirtualCurrencyDTO virtualCurrencyDTO) throws URISyntaxException {
        log.debug("REST request to save VirtualCurrency : {}", virtualCurrencyDTO);
        if (virtualCurrencyDTO.getId() != null) {
            throw new BadRequestAlertException("A new virtualCurrency cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VirtualCurrencyDTO result = virtualCurrencyService.save(virtualCurrencyDTO);
        return ResponseEntity.created(new URI("/api/virtual-currencies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /virtual-currencies : Updates an existing virtualCurrency.
     *
     * @param virtualCurrencyDTO the virtualCurrencyDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated virtualCurrencyDTO,
     * or with status 400 (Bad Request) if the virtualCurrencyDTO is not valid,
     * or with status 500 (Internal Server Error) if the virtualCurrencyDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/virtual-currencies")
    @Timed
    public ResponseEntity<VirtualCurrencyDTO> updateVirtualCurrency(@Valid @RequestBody VirtualCurrencyDTO virtualCurrencyDTO) throws URISyntaxException {
        log.debug("REST request to update VirtualCurrency : {}", virtualCurrencyDTO);
        if (virtualCurrencyDTO.getId() == null) {
            return createVirtualCurrency(virtualCurrencyDTO);
        }
        VirtualCurrencyDTO result = virtualCurrencyService.save(virtualCurrencyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, virtualCurrencyDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /virtual-currencies : get all the virtualCurrencies.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of virtualCurrencies in body
     */
    @GetMapping("/virtual-currencies")
    @Timed
    public ResponseEntity<List<VirtualCurrencyDTO>> getAllVirtualCurrencies(VirtualCurrencyCriteria criteria,@ApiParam Pageable pageable) {
        log.debug("REST request to get VirtualCurrencies by criteria: {}", criteria);
        Page<VirtualCurrencyDTO> page = virtualCurrencyQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/virtual-currencies");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /virtual-currencies/:id : get the "id" virtualCurrency.
     *
     * @param id the id of the virtualCurrencyDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the virtualCurrencyDTO, or with status 404 (Not Found)
     */
    @GetMapping("/virtual-currencies/{id}")
    @Timed
    public ResponseEntity<VirtualCurrencyDTO> getVirtualCurrency(@PathVariable Long id) {
        log.debug("REST request to get VirtualCurrency : {}", id);
        VirtualCurrencyDTO virtualCurrencyDTO = virtualCurrencyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(virtualCurrencyDTO));
    }

    /**
     * DELETE  /virtual-currencies/:id : delete the "id" virtualCurrency.
     *
     * @param id the id of the virtualCurrencyDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/virtual-currencies/{id}")
    @Timed
    public ResponseEntity<Void> deleteVirtualCurrency(@PathVariable Long id) {
        log.debug("REST request to delete VirtualCurrency : {}", id);
        virtualCurrencyService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
