import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { EventDefinition } from './event-definition.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class EventDefinitionService {

    private resourceUrl = SERVER_API_URL + 'api/event-definitions';

    constructor(private http: Http) { }

    create(eventDefinition: EventDefinition): Observable<EventDefinition> {
        const copy = this.convert(eventDefinition);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(eventDefinition: EventDefinition): Observable<EventDefinition> {
        const copy = this.convert(eventDefinition);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<EventDefinition> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to EventDefinition.
     */
    private convertItemFromServer(json: any): EventDefinition {
        const entity: EventDefinition = Object.assign(new EventDefinition(), json);
        return entity;
    }

    /**
     * Convert a EventDefinition to a JSON which can be sent to the server.
     */
    private convert(eventDefinition: EventDefinition): EventDefinition {
        const copy: EventDefinition = Object.assign({}, eventDefinition);
        return copy;
    }
}
