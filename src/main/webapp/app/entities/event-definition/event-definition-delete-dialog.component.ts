import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { EventDefinition } from './event-definition.model';
import { EventDefinitionPopupService } from './event-definition-popup.service';
import { EventDefinitionService } from './event-definition.service';

@Component({
    selector: 'jhi-event-definition-delete-dialog',
    templateUrl: './event-definition-delete-dialog.component.html'
})
export class EventDefinitionDeleteDialogComponent {

    eventDefinition: EventDefinition;

    constructor(
        private eventDefinitionService: EventDefinitionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.eventDefinitionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'eventDefinitionListModification',
                content: 'Deleted an eventDefinition'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-event-definition-delete-popup',
    template: ''
})
export class EventDefinitionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private eventDefinitionPopupService: EventDefinitionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.eventDefinitionPopupService
                .open(EventDefinitionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
