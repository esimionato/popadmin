import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PopSharedModule } from '../../shared';
import {
    EventDefinitionService,
    EventDefinitionPopupService,
    EventDefinitionComponent,
    EventDefinitionDetailComponent,
    EventDefinitionDialogComponent,
    EventDefinitionPopupComponent,
    EventDefinitionDeletePopupComponent,
    EventDefinitionDeleteDialogComponent,
    eventDefinitionRoute,
    eventDefinitionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...eventDefinitionRoute,
    ...eventDefinitionPopupRoute,
];

@NgModule({
    imports: [
        PopSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        EventDefinitionComponent,
        EventDefinitionDetailComponent,
        EventDefinitionDialogComponent,
        EventDefinitionDeleteDialogComponent,
        EventDefinitionPopupComponent,
        EventDefinitionDeletePopupComponent,
    ],
    entryComponents: [
        EventDefinitionComponent,
        EventDefinitionDialogComponent,
        EventDefinitionPopupComponent,
        EventDefinitionDeleteDialogComponent,
        EventDefinitionDeletePopupComponent,
    ],
    providers: [
        EventDefinitionService,
        EventDefinitionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PopEventDefinitionModule {}
