import { BaseEntity } from './../../shared';

export const enum EventType {
    'STATISTIC',
    'DATALOG'
}

export class EventDefinition implements BaseEntity {
    constructor(
        public id?: number,
        public evCode?: string,
        public evName?: string,
        public evDescription?: string,
        public evType?: EventType,
        public stCode?: string,
        public gameId?: number,
        public statistics?: BaseEntity[],
    ) {
    }
}
