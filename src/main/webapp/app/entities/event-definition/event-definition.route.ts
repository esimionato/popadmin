import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { EventDefinitionComponent } from './event-definition.component';
import { EventDefinitionDetailComponent } from './event-definition-detail.component';
import { EventDefinitionPopupComponent } from './event-definition-dialog.component';
import { EventDefinitionDeletePopupComponent } from './event-definition-delete-dialog.component';

export const eventDefinitionRoute: Routes = [
    {
        path: 'event-definition',
        component: EventDefinitionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.eventDefinition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'event-definition/:id',
        component: EventDefinitionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.eventDefinition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const eventDefinitionPopupRoute: Routes = [
    {
        path: 'event-definition-new',
        component: EventDefinitionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.eventDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'event-definition/:id/edit',
        component: EventDefinitionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.eventDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'event-definition/:id/delete',
        component: EventDefinitionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.eventDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
