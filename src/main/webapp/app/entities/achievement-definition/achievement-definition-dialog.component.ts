import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { AchievementDefinition } from './achievement-definition.model';
import { AchievementDefinitionPopupService } from './achievement-definition-popup.service';
import { AchievementDefinitionService } from './achievement-definition.service';
import { Game, GameService } from '../game';
import { XpLevelDefinition, XpLevelDefinitionService } from '../xp-level-definition';
import { EventDefinition, EventDefinitionService } from '../event-definition';
import { StatisticDefinition, StatisticDefinitionService } from '../statistic-definition';
import { CatalogItem, CatalogItemService } from '../catalog-item';
import { Tournament, TournamentService } from '../tournament';
import { VirtualCurrency, VirtualCurrencyService } from '../virtual-currency';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-achievement-definition-dialog',
    templateUrl: './achievement-definition-dialog.component.html'
})
export class AchievementDefinitionDialogComponent implements OnInit {

    achievementDefinition: AchievementDefinition;
    isSaving: boolean;

    games: Game[];

    xpleveldefinitions: XpLevelDefinition[];

    eventdefinitions: EventDefinition[];

    statisticdefinitions: StatisticDefinition[];

    catalogitems: CatalogItem[];

    tournaments: Tournament[];

    virtualcurrencies: VirtualCurrency[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private achievementDefinitionService: AchievementDefinitionService,
        private gameService: GameService,
        private xpLevelDefinitionService: XpLevelDefinitionService,
        private eventDefinitionService: EventDefinitionService,
        private statisticDefinitionService: StatisticDefinitionService,
        private catalogItemService: CatalogItemService,
        private tournamentService: TournamentService,
        private virtualCurrencyService: VirtualCurrencyService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.gameService.query()
            .subscribe((res: ResponseWrapper) => { this.games = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.xpLevelDefinitionService.query()
            .subscribe((res: ResponseWrapper) => { this.xpleveldefinitions = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.eventDefinitionService.query()
            .subscribe((res: ResponseWrapper) => { this.eventdefinitions = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.statisticDefinitionService.query()
            .subscribe((res: ResponseWrapper) => { this.statisticdefinitions = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.catalogItemService.query()
            .subscribe((res: ResponseWrapper) => { this.catalogitems = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.tournamentService.query()
            .subscribe((res: ResponseWrapper) => { this.tournaments = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
        this.virtualCurrencyService.query()
            .subscribe((res: ResponseWrapper) => { this.virtualcurrencies = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.achievementDefinition.id !== undefined) {
            this.subscribeToSaveResponse(
                this.achievementDefinitionService.update(this.achievementDefinition));
        } else {
            this.subscribeToSaveResponse(
                this.achievementDefinitionService.create(this.achievementDefinition));
        }
    }

    private subscribeToSaveResponse(result: Observable<AchievementDefinition>) {
        result.subscribe((res: AchievementDefinition) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: AchievementDefinition) {
        this.eventManager.broadcast({ name: 'achievementDefinitionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackGameById(index: number, item: Game) {
        return item.id;
    }

    trackXpLevelDefinitionById(index: number, item: XpLevelDefinition) {
        return item.id;
    }

    trackEventDefinitionById(index: number, item: EventDefinition) {
        return item.id;
    }

    trackStatisticDefinitionById(index: number, item: StatisticDefinition) {
        return item.id;
    }

    trackCatalogItemById(index: number, item: CatalogItem) {
        return item.id;
    }

    trackTournamentById(index: number, item: Tournament) {
        return item.id;
    }

    trackVirtualCurrencyById(index: number, item: VirtualCurrency) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-achievement-definition-popup',
    template: ''
})
export class AchievementDefinitionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private achievementDefinitionPopupService: AchievementDefinitionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.achievementDefinitionPopupService
                    .open(AchievementDefinitionDialogComponent as Component, params['id']);
            } else {
                this.achievementDefinitionPopupService
                    .open(AchievementDefinitionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
