import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { AchievementDefinition } from './achievement-definition.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class AchievementDefinitionService {

    private resourceUrl = SERVER_API_URL + 'api/achievement-definitions';

    constructor(private http: Http) { }

    create(achievementDefinition: AchievementDefinition): Observable<AchievementDefinition> {
        const copy = this.convert(achievementDefinition);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(achievementDefinition: AchievementDefinition): Observable<AchievementDefinition> {
        const copy = this.convert(achievementDefinition);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<AchievementDefinition> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to AchievementDefinition.
     */
    private convertItemFromServer(json: any): AchievementDefinition {
        const entity: AchievementDefinition = Object.assign(new AchievementDefinition(), json);
        return entity;
    }

    /**
     * Convert a AchievementDefinition to a JSON which can be sent to the server.
     */
    private convert(achievementDefinition: AchievementDefinition): AchievementDefinition {
        const copy: AchievementDefinition = Object.assign({}, achievementDefinition);
        return copy;
    }
}
