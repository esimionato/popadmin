import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { AchievementDefinitionComponent } from './achievement-definition.component';
import { AchievementDefinitionDetailComponent } from './achievement-definition-detail.component';
import { AchievementDefinitionPopupComponent } from './achievement-definition-dialog.component';
import { AchievementDefinitionDeletePopupComponent } from './achievement-definition-delete-dialog.component';

export const achievementDefinitionRoute: Routes = [
    {
        path: 'achievement-definition',
        component: AchievementDefinitionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.achievementDefinition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'achievement-definition/:id',
        component: AchievementDefinitionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.achievementDefinition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const achievementDefinitionPopupRoute: Routes = [
    {
        path: 'achievement-definition-new',
        component: AchievementDefinitionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.achievementDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'achievement-definition/:id/edit',
        component: AchievementDefinitionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.achievementDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'achievement-definition/:id/delete',
        component: AchievementDefinitionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.achievementDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
