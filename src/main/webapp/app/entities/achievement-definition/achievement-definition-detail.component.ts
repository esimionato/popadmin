import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { AchievementDefinition } from './achievement-definition.model';
import { AchievementDefinitionService } from './achievement-definition.service';

@Component({
    selector: 'jhi-achievement-definition-detail',
    templateUrl: './achievement-definition-detail.component.html'
})
export class AchievementDefinitionDetailComponent implements OnInit, OnDestroy {

    achievementDefinition: AchievementDefinition;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private achievementDefinitionService: AchievementDefinitionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInAchievementDefinitions();
    }

    load(id) {
        this.achievementDefinitionService.find(id).subscribe((achievementDefinition) => {
            this.achievementDefinition = achievementDefinition;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInAchievementDefinitions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'achievementDefinitionListModification',
            (response) => this.load(this.achievementDefinition.id)
        );
    }
}
