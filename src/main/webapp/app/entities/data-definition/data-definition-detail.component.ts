import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { DataDefinition } from './data-definition.model';
import { DataDefinitionService } from './data-definition.service';

@Component({
    selector: 'jhi-data-definition-detail',
    templateUrl: './data-definition-detail.component.html'
})
export class DataDefinitionDetailComponent implements OnInit, OnDestroy {

    dataDefinition: DataDefinition;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dataDefinitionService: DataDefinitionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDataDefinitions();
    }

    load(id) {
        this.dataDefinitionService.find(id).subscribe((dataDefinition) => {
            this.dataDefinition = dataDefinition;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDataDefinitions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dataDefinitionListModification',
            (response) => this.load(this.dataDefinition.id)
        );
    }
}
