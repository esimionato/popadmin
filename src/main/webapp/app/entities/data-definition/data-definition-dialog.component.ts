import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { DataDefinition } from './data-definition.model';
import { DataDefinitionPopupService } from './data-definition-popup.service';
import { DataDefinitionService } from './data-definition.service';
import { Game, GameService } from '../game';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-data-definition-dialog',
    templateUrl: './data-definition-dialog.component.html'
})
export class DataDefinitionDialogComponent implements OnInit {

    dataDefinition: DataDefinition;
    isSaving: boolean;

    games: Game[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private dataDefinitionService: DataDefinitionService,
        private gameService: GameService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.gameService.query()
            .subscribe((res: ResponseWrapper) => { this.games = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dataDefinition.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dataDefinitionService.update(this.dataDefinition));
        } else {
            this.subscribeToSaveResponse(
                this.dataDefinitionService.create(this.dataDefinition));
        }
    }

    private subscribeToSaveResponse(result: Observable<DataDefinition>) {
        result.subscribe((res: DataDefinition) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: DataDefinition) {
        this.eventManager.broadcast({ name: 'dataDefinitionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackGameById(index: number, item: Game) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-data-definition-popup',
    template: ''
})
export class DataDefinitionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dataDefinitionPopupService: DataDefinitionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dataDefinitionPopupService
                    .open(DataDefinitionDialogComponent as Component, params['id']);
            } else {
                this.dataDefinitionPopupService
                    .open(DataDefinitionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
