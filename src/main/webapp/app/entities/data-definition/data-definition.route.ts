import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { DataDefinitionComponent } from './data-definition.component';
import { DataDefinitionDetailComponent } from './data-definition-detail.component';
import { DataDefinitionPopupComponent } from './data-definition-dialog.component';
import { DataDefinitionDeletePopupComponent } from './data-definition-delete-dialog.component';

export const dataDefinitionRoute: Routes = [
    {
        path: 'data-definition',
        component: DataDefinitionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.dataDefinition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'data-definition/:id',
        component: DataDefinitionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.dataDefinition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dataDefinitionPopupRoute: Routes = [
    {
        path: 'data-definition-new',
        component: DataDefinitionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.dataDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-definition/:id/edit',
        component: DataDefinitionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.dataDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'data-definition/:id/delete',
        component: DataDefinitionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.dataDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
