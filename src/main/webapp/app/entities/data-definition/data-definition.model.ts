import { BaseEntity } from './../../shared';

export const enum DataKlass {
    'GAMMING_RULES',
    'PROGRESION',
    'METADATA',
    'SKILL',
    'PERFIL'
}

export const enum DataScope {
    'PUBLIC',
    'PRIVATE',
    'INTERNAL'
}

export const enum DataPermission {
    'WRITE',
    'ONLY_READ'
}

export class DataDefinition implements BaseEntity {
    constructor(
        public id?: number,
        public dataIconUrl?: string,
        public dataKey?: string,
        public dataName?: string,
        public description?: string,
        public defaultValue?: string,
        public dataKlass?: DataKlass,
        public scope?: DataScope,
        public permissions?: DataPermission,
        public fixed?: boolean,
        public dataSpace?: string,
        public gameId?: number,
    ) {
        this.fixed = false;
    }
}
