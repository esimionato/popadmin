import { BaseEntity } from './../../shared';

export class PlayerData implements BaseEntity {
    constructor(
        public id?: number,
        public dataValue?: string,
        public lastModifiedAt?: any,
        public dataVersion?: string,
        public createdAt?: any,
        public playerId?: number,
        public data_definitionId?: number,
    ) {
    }
}
