import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PopSharedModule } from '../../shared';
import {
    PlayerDataService,
    PlayerDataPopupService,
    PlayerDataComponent,
    PlayerDataDetailComponent,
    PlayerDataDialogComponent,
    PlayerDataPopupComponent,
    PlayerDataDeletePopupComponent,
    PlayerDataDeleteDialogComponent,
    playerDataRoute,
    playerDataPopupRoute,
} from './';

const ENTITY_STATES = [
    ...playerDataRoute,
    ...playerDataPopupRoute,
];

@NgModule({
    imports: [
        PopSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PlayerDataComponent,
        PlayerDataDetailComponent,
        PlayerDataDialogComponent,
        PlayerDataDeleteDialogComponent,
        PlayerDataPopupComponent,
        PlayerDataDeletePopupComponent,
    ],
    entryComponents: [
        PlayerDataComponent,
        PlayerDataDialogComponent,
        PlayerDataPopupComponent,
        PlayerDataDeleteDialogComponent,
        PlayerDataDeletePopupComponent,
    ],
    providers: [
        PlayerDataService,
        PlayerDataPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PopPlayerDataModule {}
