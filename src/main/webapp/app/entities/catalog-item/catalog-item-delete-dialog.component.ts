import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CatalogItem } from './catalog-item.model';
import { CatalogItemPopupService } from './catalog-item-popup.service';
import { CatalogItemService } from './catalog-item.service';

@Component({
    selector: 'jhi-catalog-item-delete-dialog',
    templateUrl: './catalog-item-delete-dialog.component.html'
})
export class CatalogItemDeleteDialogComponent {

    catalogItem: CatalogItem;

    constructor(
        private catalogItemService: CatalogItemService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.catalogItemService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'catalogItemListModification',
                content: 'Deleted an catalogItem'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-catalog-item-delete-popup',
    template: ''
})
export class CatalogItemDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private catalogItemPopupService: CatalogItemPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.catalogItemPopupService
                .open(CatalogItemDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
