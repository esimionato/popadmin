import { BaseEntity } from './../../shared';

export class PlayerVcurrencyBalance implements BaseEntity {
    constructor(
        public id?: number,
        public amount?: number,
        public playerId?: number,
        public virtual_currencyId?: number,
    ) {
    }
}
