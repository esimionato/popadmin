import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PlayerVcurrencyBalance } from './player-vcurrency-balance.model';
import { PlayerVcurrencyBalancePopupService } from './player-vcurrency-balance-popup.service';
import { PlayerVcurrencyBalanceService } from './player-vcurrency-balance.service';

@Component({
    selector: 'jhi-player-vcurrency-balance-delete-dialog',
    templateUrl: './player-vcurrency-balance-delete-dialog.component.html'
})
export class PlayerVcurrencyBalanceDeleteDialogComponent {

    playerVcurrencyBalance: PlayerVcurrencyBalance;

    constructor(
        private playerVcurrencyBalanceService: PlayerVcurrencyBalanceService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.playerVcurrencyBalanceService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'playerVcurrencyBalanceListModification',
                content: 'Deleted an playerVcurrencyBalance'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-player-vcurrency-balance-delete-popup',
    template: ''
})
export class PlayerVcurrencyBalanceDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private playerVcurrencyBalancePopupService: PlayerVcurrencyBalancePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.playerVcurrencyBalancePopupService
                .open(PlayerVcurrencyBalanceDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
