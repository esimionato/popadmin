import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { PlayerVcurrencyBalance } from './player-vcurrency-balance.model';
import { PlayerVcurrencyBalanceService } from './player-vcurrency-balance.service';

@Component({
    selector: 'jhi-player-vcurrency-balance-detail',
    templateUrl: './player-vcurrency-balance-detail.component.html'
})
export class PlayerVcurrencyBalanceDetailComponent implements OnInit, OnDestroy {

    playerVcurrencyBalance: PlayerVcurrencyBalance;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private playerVcurrencyBalanceService: PlayerVcurrencyBalanceService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPlayerVcurrencyBalances();
    }

    load(id) {
        this.playerVcurrencyBalanceService.find(id).subscribe((playerVcurrencyBalance) => {
            this.playerVcurrencyBalance = playerVcurrencyBalance;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPlayerVcurrencyBalances() {
        this.eventSubscriber = this.eventManager.subscribe(
            'playerVcurrencyBalanceListModification',
            (response) => this.load(this.playerVcurrencyBalance.id)
        );
    }
}
