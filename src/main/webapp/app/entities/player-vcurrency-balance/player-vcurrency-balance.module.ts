import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PopSharedModule } from '../../shared';
import {
    PlayerVcurrencyBalanceService,
    PlayerVcurrencyBalancePopupService,
    PlayerVcurrencyBalanceComponent,
    PlayerVcurrencyBalanceDetailComponent,
    PlayerVcurrencyBalanceDialogComponent,
    PlayerVcurrencyBalancePopupComponent,
    PlayerVcurrencyBalanceDeletePopupComponent,
    PlayerVcurrencyBalanceDeleteDialogComponent,
    playerVcurrencyBalanceRoute,
    playerVcurrencyBalancePopupRoute,
} from './';

const ENTITY_STATES = [
    ...playerVcurrencyBalanceRoute,
    ...playerVcurrencyBalancePopupRoute,
];

@NgModule({
    imports: [
        PopSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PlayerVcurrencyBalanceComponent,
        PlayerVcurrencyBalanceDetailComponent,
        PlayerVcurrencyBalanceDialogComponent,
        PlayerVcurrencyBalanceDeleteDialogComponent,
        PlayerVcurrencyBalancePopupComponent,
        PlayerVcurrencyBalanceDeletePopupComponent,
    ],
    entryComponents: [
        PlayerVcurrencyBalanceComponent,
        PlayerVcurrencyBalanceDialogComponent,
        PlayerVcurrencyBalancePopupComponent,
        PlayerVcurrencyBalanceDeleteDialogComponent,
        PlayerVcurrencyBalanceDeletePopupComponent,
    ],
    providers: [
        PlayerVcurrencyBalanceService,
        PlayerVcurrencyBalancePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PopPlayerVcurrencyBalanceModule {}
