import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PlayerVcurrencyBalanceComponent } from './player-vcurrency-balance.component';
import { PlayerVcurrencyBalanceDetailComponent } from './player-vcurrency-balance-detail.component';
import { PlayerVcurrencyBalancePopupComponent } from './player-vcurrency-balance-dialog.component';
import { PlayerVcurrencyBalanceDeletePopupComponent } from './player-vcurrency-balance-delete-dialog.component';

export const playerVcurrencyBalanceRoute: Routes = [
    {
        path: 'player-vcurrency-balance',
        component: PlayerVcurrencyBalanceComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.playerVcurrencyBalance.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'player-vcurrency-balance/:id',
        component: PlayerVcurrencyBalanceDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.playerVcurrencyBalance.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const playerVcurrencyBalancePopupRoute: Routes = [
    {
        path: 'player-vcurrency-balance-new',
        component: PlayerVcurrencyBalancePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.playerVcurrencyBalance.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'player-vcurrency-balance/:id/edit',
        component: PlayerVcurrencyBalancePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.playerVcurrencyBalance.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'player-vcurrency-balance/:id/delete',
        component: PlayerVcurrencyBalanceDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.playerVcurrencyBalance.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
