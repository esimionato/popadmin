import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PlayerVcurrencyBalance } from './player-vcurrency-balance.model';
import { PlayerVcurrencyBalanceService } from './player-vcurrency-balance.service';

@Injectable()
export class PlayerVcurrencyBalancePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private playerVcurrencyBalanceService: PlayerVcurrencyBalanceService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.playerVcurrencyBalanceService.find(id).subscribe((playerVcurrencyBalance) => {
                    this.ngbModalRef = this.playerVcurrencyBalanceModalRef(component, playerVcurrencyBalance);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.playerVcurrencyBalanceModalRef(component, new PlayerVcurrencyBalance());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    playerVcurrencyBalanceModalRef(component: Component, playerVcurrencyBalance: PlayerVcurrencyBalance): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.playerVcurrencyBalance = playerVcurrencyBalance;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
