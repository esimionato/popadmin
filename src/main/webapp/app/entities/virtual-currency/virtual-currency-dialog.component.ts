import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VirtualCurrency } from './virtual-currency.model';
import { VirtualCurrencyPopupService } from './virtual-currency-popup.service';
import { VirtualCurrencyService } from './virtual-currency.service';
import { Game, GameService } from '../game';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-virtual-currency-dialog',
    templateUrl: './virtual-currency-dialog.component.html'
})
export class VirtualCurrencyDialogComponent implements OnInit {

    virtualCurrency: VirtualCurrency;
    isSaving: boolean;

    games: Game[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private virtualCurrencyService: VirtualCurrencyService,
        private gameService: GameService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.gameService.query()
            .subscribe((res: ResponseWrapper) => { this.games = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.virtualCurrency.id !== undefined) {
            this.subscribeToSaveResponse(
                this.virtualCurrencyService.update(this.virtualCurrency));
        } else {
            this.subscribeToSaveResponse(
                this.virtualCurrencyService.create(this.virtualCurrency));
        }
    }

    private subscribeToSaveResponse(result: Observable<VirtualCurrency>) {
        result.subscribe((res: VirtualCurrency) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: VirtualCurrency) {
        this.eventManager.broadcast({ name: 'virtualCurrencyListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackGameById(index: number, item: Game) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-virtual-currency-popup',
    template: ''
})
export class VirtualCurrencyPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private virtualCurrencyPopupService: VirtualCurrencyPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.virtualCurrencyPopupService
                    .open(VirtualCurrencyDialogComponent as Component, params['id']);
            } else {
                this.virtualCurrencyPopupService
                    .open(VirtualCurrencyDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
