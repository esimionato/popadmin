import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PopSharedModule } from '../../shared';
import {
    VirtualCurrencyService,
    VirtualCurrencyPopupService,
    VirtualCurrencyComponent,
    VirtualCurrencyDetailComponent,
    VirtualCurrencyDialogComponent,
    VirtualCurrencyPopupComponent,
    VirtualCurrencyDeletePopupComponent,
    VirtualCurrencyDeleteDialogComponent,
    virtualCurrencyRoute,
    virtualCurrencyPopupRoute,
} from './';

const ENTITY_STATES = [
    ...virtualCurrencyRoute,
    ...virtualCurrencyPopupRoute,
];

@NgModule({
    imports: [
        PopSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        VirtualCurrencyComponent,
        VirtualCurrencyDetailComponent,
        VirtualCurrencyDialogComponent,
        VirtualCurrencyDeleteDialogComponent,
        VirtualCurrencyPopupComponent,
        VirtualCurrencyDeletePopupComponent,
    ],
    entryComponents: [
        VirtualCurrencyComponent,
        VirtualCurrencyDialogComponent,
        VirtualCurrencyPopupComponent,
        VirtualCurrencyDeleteDialogComponent,
        VirtualCurrencyDeletePopupComponent,
    ],
    providers: [
        VirtualCurrencyService,
        VirtualCurrencyPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PopVirtualCurrencyModule {}
