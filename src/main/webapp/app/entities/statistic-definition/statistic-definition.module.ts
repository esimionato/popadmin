import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PopSharedModule } from '../../shared';
import {
    StatisticDefinitionService,
    StatisticDefinitionPopupService,
    StatisticDefinitionComponent,
    StatisticDefinitionDetailComponent,
    StatisticDefinitionDialogComponent,
    StatisticDefinitionPopupComponent,
    StatisticDefinitionDeletePopupComponent,
    StatisticDefinitionDeleteDialogComponent,
    statisticDefinitionRoute,
    statisticDefinitionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...statisticDefinitionRoute,
    ...statisticDefinitionPopupRoute,
];

@NgModule({
    imports: [
        PopSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        StatisticDefinitionComponent,
        StatisticDefinitionDetailComponent,
        StatisticDefinitionDialogComponent,
        StatisticDefinitionDeleteDialogComponent,
        StatisticDefinitionPopupComponent,
        StatisticDefinitionDeletePopupComponent,
    ],
    entryComponents: [
        StatisticDefinitionComponent,
        StatisticDefinitionDialogComponent,
        StatisticDefinitionPopupComponent,
        StatisticDefinitionDeleteDialogComponent,
        StatisticDefinitionDeletePopupComponent,
    ],
    providers: [
        StatisticDefinitionService,
        StatisticDefinitionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PopStatisticDefinitionModule {}
