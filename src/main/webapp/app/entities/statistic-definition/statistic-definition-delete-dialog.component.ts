import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { StatisticDefinition } from './statistic-definition.model';
import { StatisticDefinitionPopupService } from './statistic-definition-popup.service';
import { StatisticDefinitionService } from './statistic-definition.service';

@Component({
    selector: 'jhi-statistic-definition-delete-dialog',
    templateUrl: './statistic-definition-delete-dialog.component.html'
})
export class StatisticDefinitionDeleteDialogComponent {

    statisticDefinition: StatisticDefinition;

    constructor(
        private statisticDefinitionService: StatisticDefinitionService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.statisticDefinitionService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'statisticDefinitionListModification',
                content: 'Deleted an statisticDefinition'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-statistic-definition-delete-popup',
    template: ''
})
export class StatisticDefinitionDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private statisticDefinitionPopupService: StatisticDefinitionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.statisticDefinitionPopupService
                .open(StatisticDefinitionDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
