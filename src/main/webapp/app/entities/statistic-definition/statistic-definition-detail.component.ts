import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { StatisticDefinition } from './statistic-definition.model';
import { StatisticDefinitionService } from './statistic-definition.service';

@Component({
    selector: 'jhi-statistic-definition-detail',
    templateUrl: './statistic-definition-detail.component.html'
})
export class StatisticDefinitionDetailComponent implements OnInit, OnDestroy {

    statisticDefinition: StatisticDefinition;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private statisticDefinitionService: StatisticDefinitionService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInStatisticDefinitions();
    }

    load(id) {
        this.statisticDefinitionService.find(id).subscribe((statisticDefinition) => {
            this.statisticDefinition = statisticDefinition;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInStatisticDefinitions() {
        this.eventSubscriber = this.eventManager.subscribe(
            'statisticDefinitionListModification',
            (response) => this.load(this.statisticDefinition.id)
        );
    }
}
