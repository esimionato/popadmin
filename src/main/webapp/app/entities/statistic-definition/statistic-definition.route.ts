import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { StatisticDefinitionComponent } from './statistic-definition.component';
import { StatisticDefinitionDetailComponent } from './statistic-definition-detail.component';
import { StatisticDefinitionPopupComponent } from './statistic-definition-dialog.component';
import { StatisticDefinitionDeletePopupComponent } from './statistic-definition-delete-dialog.component';

export const statisticDefinitionRoute: Routes = [
    {
        path: 'statistic-definition',
        component: StatisticDefinitionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.statisticDefinition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'statistic-definition/:id',
        component: StatisticDefinitionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.statisticDefinition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const statisticDefinitionPopupRoute: Routes = [
    {
        path: 'statistic-definition-new',
        component: StatisticDefinitionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.statisticDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'statistic-definition/:id/edit',
        component: StatisticDefinitionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.statisticDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'statistic-definition/:id/delete',
        component: StatisticDefinitionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'popApp.statisticDefinition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
