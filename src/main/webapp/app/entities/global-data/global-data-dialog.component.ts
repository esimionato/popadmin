import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { GlobalData } from './global-data.model';
import { GlobalDataPopupService } from './global-data-popup.service';
import { GlobalDataService } from './global-data.service';
import { Game, GameService } from '../game';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-global-data-dialog',
    templateUrl: './global-data-dialog.component.html'
})
export class GlobalDataDialogComponent implements OnInit {

    globalData: GlobalData;
    isSaving: boolean;

    games: Game[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private globalDataService: GlobalDataService,
        private gameService: GameService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.gameService.query()
            .subscribe((res: ResponseWrapper) => { this.games = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.globalData.id !== undefined) {
            this.subscribeToSaveResponse(
                this.globalDataService.update(this.globalData));
        } else {
            this.subscribeToSaveResponse(
                this.globalDataService.create(this.globalData));
        }
    }

    private subscribeToSaveResponse(result: Observable<GlobalData>) {
        result.subscribe((res: GlobalData) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: GlobalData) {
        this.eventManager.broadcast({ name: 'globalDataListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackGameById(index: number, item: Game) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-global-data-popup',
    template: ''
})
export class GlobalDataPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private globalDataPopupService: GlobalDataPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.globalDataPopupService
                    .open(GlobalDataDialogComponent as Component, params['id']);
            } else {
                this.globalDataPopupService
                    .open(GlobalDataDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
