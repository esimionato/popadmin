import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PopSharedModule } from '../../shared';
import {
    GlobalDataService,
    GlobalDataPopupService,
    GlobalDataComponent,
    GlobalDataDetailComponent,
    GlobalDataDialogComponent,
    GlobalDataPopupComponent,
    GlobalDataDeletePopupComponent,
    GlobalDataDeleteDialogComponent,
    globalDataRoute,
    globalDataPopupRoute,
} from './';

const ENTITY_STATES = [
    ...globalDataRoute,
    ...globalDataPopupRoute,
];

@NgModule({
    imports: [
        PopSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        GlobalDataComponent,
        GlobalDataDetailComponent,
        GlobalDataDialogComponent,
        GlobalDataDeleteDialogComponent,
        GlobalDataPopupComponent,
        GlobalDataDeletePopupComponent,
    ],
    entryComponents: [
        GlobalDataComponent,
        GlobalDataDialogComponent,
        GlobalDataPopupComponent,
        GlobalDataDeleteDialogComponent,
        GlobalDataDeletePopupComponent,
    ],
    providers: [
        GlobalDataService,
        GlobalDataPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PopGlobalDataModule {}
