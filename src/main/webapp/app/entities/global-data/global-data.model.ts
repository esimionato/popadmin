import { BaseEntity } from './../../shared';

export const enum DataScope {
    'PUBLIC',
    'PRIVATE',
    'INTERNAL'
}

export const enum DataPermission {
    'WRITE',
    'ONLY_READ'
}

export class GlobalData implements BaseEntity {
    constructor(
        public id?: number,
        public dataKey?: string,
        public dataValue?: string,
        public scope?: DataScope,
        public permissions?: DataPermission,
        public dataVersion?: number,
        public createdAt?: any,
        public lastModifiedAt?: any,
        public dataSpace?: string,
        public gameId?: number,
    ) {
    }
}
