import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { GlobalData } from './global-data.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class GlobalDataService {

    private resourceUrl = SERVER_API_URL + 'api/global-data';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(globalData: GlobalData): Observable<GlobalData> {
        const copy = this.convert(globalData);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    update(globalData: GlobalData): Observable<GlobalData> {
        const copy = this.convert(globalData);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    find(id: number): Observable<GlobalData> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            return this.convertItemFromServer(jsonResponse);
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        const result = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            result.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return new ResponseWrapper(res.headers, result, res.status);
    }

    /**
     * Convert a returned JSON object to GlobalData.
     */
    private convertItemFromServer(json: any): GlobalData {
        const entity: GlobalData = Object.assign(new GlobalData(), json);
        entity.createdAt = this.dateUtils
            .convertDateTimeFromServer(json.createdAt);
        entity.lastModifiedAt = this.dateUtils
            .convertDateTimeFromServer(json.lastModifiedAt);
        return entity;
    }

    /**
     * Convert a GlobalData to a JSON which can be sent to the server.
     */
    private convert(globalData: GlobalData): GlobalData {
        const copy: GlobalData = Object.assign({}, globalData);

        copy.createdAt = this.dateUtils.toDate(globalData.createdAt);

        copy.lastModifiedAt = this.dateUtils.toDate(globalData.lastModifiedAt);
        return copy;
    }
}
