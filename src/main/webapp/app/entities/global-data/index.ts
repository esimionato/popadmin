export * from './global-data.model';
export * from './global-data-popup.service';
export * from './global-data.service';
export * from './global-data-dialog.component';
export * from './global-data-delete-dialog.component';
export * from './global-data-detail.component';
export * from './global-data.component';
export * from './global-data.route';
