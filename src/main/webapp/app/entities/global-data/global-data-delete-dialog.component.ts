import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { GlobalData } from './global-data.model';
import { GlobalDataPopupService } from './global-data-popup.service';
import { GlobalDataService } from './global-data.service';

@Component({
    selector: 'jhi-global-data-delete-dialog',
    templateUrl: './global-data-delete-dialog.component.html'
})
export class GlobalDataDeleteDialogComponent {

    globalData: GlobalData;

    constructor(
        private globalDataService: GlobalDataService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.globalDataService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'globalDataListModification',
                content: 'Deleted an globalData'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-global-data-delete-popup',
    template: ''
})
export class GlobalDataDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private globalDataPopupService: GlobalDataPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.globalDataPopupService
                .open(GlobalDataDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
