import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { GameStatistic } from './game-statistic.model';
import { GameStatisticPopupService } from './game-statistic-popup.service';
import { GameStatisticService } from './game-statistic.service';
import { Game, GameService } from '../game';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-game-statistic-dialog',
    templateUrl: './game-statistic-dialog.component.html'
})
export class GameStatisticDialogComponent implements OnInit {

    gameStatistic: GameStatistic;
    isSaving: boolean;

    games: Game[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private gameStatisticService: GameStatisticService,
        private gameService: GameService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.gameService.query()
            .subscribe((res: ResponseWrapper) => { this.games = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.gameStatistic.id !== undefined) {
            this.subscribeToSaveResponse(
                this.gameStatisticService.update(this.gameStatistic));
        } else {
            this.subscribeToSaveResponse(
                this.gameStatisticService.create(this.gameStatistic));
        }
    }

    private subscribeToSaveResponse(result: Observable<GameStatistic>) {
        result.subscribe((res: GameStatistic) =>
            this.onSaveSuccess(res), (res: Response) => this.onSaveError());
    }

    private onSaveSuccess(result: GameStatistic) {
        this.eventManager.broadcast({ name: 'gameStatisticListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackGameById(index: number, item: Game) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-game-statistic-popup',
    template: ''
})
export class GameStatisticPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private gameStatisticPopupService: GameStatisticPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.gameStatisticPopupService
                    .open(GameStatisticDialogComponent as Component, params['id']);
            } else {
                this.gameStatisticPopupService
                    .open(GameStatisticDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
