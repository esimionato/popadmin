export * from './game-statistic.model';
export * from './game-statistic-popup.service';
export * from './game-statistic.service';
export * from './game-statistic-dialog.component';
export * from './game-statistic-delete-dialog.component';
export * from './game-statistic-detail.component';
export * from './game-statistic.component';
export * from './game-statistic.route';
