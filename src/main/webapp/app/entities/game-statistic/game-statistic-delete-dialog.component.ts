import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { GameStatistic } from './game-statistic.model';
import { GameStatisticPopupService } from './game-statistic-popup.service';
import { GameStatisticService } from './game-statistic.service';

@Component({
    selector: 'jhi-game-statistic-delete-dialog',
    templateUrl: './game-statistic-delete-dialog.component.html'
})
export class GameStatisticDeleteDialogComponent {

    gameStatistic: GameStatistic;

    constructor(
        private gameStatisticService: GameStatisticService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.gameStatisticService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'gameStatisticListModification',
                content: 'Deleted an gameStatistic'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-game-statistic-delete-popup',
    template: ''
})
export class GameStatisticDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private gameStatisticPopupService: GameStatisticPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.gameStatisticPopupService
                .open(GameStatisticDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
