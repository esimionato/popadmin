import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager } from 'ng-jhipster';

import { GameStatistic } from './game-statistic.model';
import { GameStatisticService } from './game-statistic.service';

@Component({
    selector: 'jhi-game-statistic-detail',
    templateUrl: './game-statistic-detail.component.html'
})
export class GameStatisticDetailComponent implements OnInit, OnDestroy {

    gameStatistic: GameStatistic;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private gameStatisticService: GameStatisticService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInGameStatistics();
    }

    load(id) {
        this.gameStatisticService.find(id).subscribe((gameStatistic) => {
            this.gameStatistic = gameStatistic;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInGameStatistics() {
        this.eventSubscriber = this.eventManager.subscribe(
            'gameStatisticListModification',
            (response) => this.load(this.gameStatistic.id)
        );
    }
}
