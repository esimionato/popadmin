import { BaseEntity } from './../../shared';

export const enum PlayerStatus {
    'ACTIVE',
    'BANNED'
}

export class Player implements BaseEntity {
    constructor(
        public id?: number,
        public popId?: string,
        public deviceId?: string,
        public username?: string,
        public email?: string,
        public avatarUrl?: string,
        public status?: PlayerStatus,
        public online?: boolean,
        public createdAt?: any,
        public lastModifiedAt?: any,
        public bannedUntilAt?: any,
        public gameId?: number,
    ) {
        this.online = false;
    }
}
