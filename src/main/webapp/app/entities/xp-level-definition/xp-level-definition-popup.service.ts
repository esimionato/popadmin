import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { XpLevelDefinition } from './xp-level-definition.model';
import { XpLevelDefinitionService } from './xp-level-definition.service';

@Injectable()
export class XpLevelDefinitionPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private xpLevelDefinitionService: XpLevelDefinitionService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.xpLevelDefinitionService.find(id).subscribe((xpLevelDefinition) => {
                    this.ngbModalRef = this.xpLevelDefinitionModalRef(component, xpLevelDefinition);
                    resolve(this.ngbModalRef);
                });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.xpLevelDefinitionModalRef(component, new XpLevelDefinition());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    xpLevelDefinitionModalRef(component: Component, xpLevelDefinition: XpLevelDefinition): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.xpLevelDefinition = xpLevelDefinition;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
