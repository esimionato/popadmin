import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PopSharedModule } from '../../shared';
import {
    XpLevelDefinitionService,
    XpLevelDefinitionPopupService,
    XpLevelDefinitionComponent,
    XpLevelDefinitionDetailComponent,
    XpLevelDefinitionDialogComponent,
    XpLevelDefinitionPopupComponent,
    XpLevelDefinitionDeletePopupComponent,
    XpLevelDefinitionDeleteDialogComponent,
    xpLevelDefinitionRoute,
    xpLevelDefinitionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...xpLevelDefinitionRoute,
    ...xpLevelDefinitionPopupRoute,
];

@NgModule({
    imports: [
        PopSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        XpLevelDefinitionComponent,
        XpLevelDefinitionDetailComponent,
        XpLevelDefinitionDialogComponent,
        XpLevelDefinitionDeleteDialogComponent,
        XpLevelDefinitionPopupComponent,
        XpLevelDefinitionDeletePopupComponent,
    ],
    entryComponents: [
        XpLevelDefinitionComponent,
        XpLevelDefinitionDialogComponent,
        XpLevelDefinitionPopupComponent,
        XpLevelDefinitionDeleteDialogComponent,
        XpLevelDefinitionDeletePopupComponent,
    ],
    providers: [
        XpLevelDefinitionService,
        XpLevelDefinitionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PopXpLevelDefinitionModule {}
