import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PopPlayerModule } from './player/player.module';
import { PopGameModule } from './game/game.module';
import { PopDataDefinitionModule } from './data-definition/data-definition.module';
import { PopPlayerDataModule } from './player-data/player-data.module';
import { PopVirtualCurrencyModule } from './virtual-currency/virtual-currency.module';
import { PopPlayerVcurrencyBalanceModule } from './player-vcurrency-balance/player-vcurrency-balance.module';
import { PopCatalogItemModule } from './catalog-item/catalog-item.module';
import { PopGlobalDataModule } from './global-data/global-data.module';
import { PopStatisticDefinitionModule } from './statistic-definition/statistic-definition.module';
import { PopGameStatisticModule } from './game-statistic/game-statistic.module';
import { PopEventDefinitionModule } from './event-definition/event-definition.module';
import { PopXpLevelDefinitionModule } from './xp-level-definition/xp-level-definition.module';
import { PopTournamentModule } from './tournament/tournament.module';
import { PopAchievementDefinitionModule } from './achievement-definition/achievement-definition.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        PopPlayerModule,
        PopGameModule,
        PopDataDefinitionModule,
        PopPlayerDataModule,
        PopVirtualCurrencyModule,
        PopPlayerVcurrencyBalanceModule,
        PopCatalogItemModule,
        PopGlobalDataModule,
        PopStatisticDefinitionModule,
        PopGameStatisticModule,
        PopEventDefinitionModule,
        PopXpLevelDefinitionModule,
        PopTournamentModule,
        PopAchievementDefinitionModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PopEntityModule {}
