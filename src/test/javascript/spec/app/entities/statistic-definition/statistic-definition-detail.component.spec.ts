/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PopTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { StatisticDefinitionDetailComponent } from '../../../../../../main/webapp/app/entities/statistic-definition/statistic-definition-detail.component';
import { StatisticDefinitionService } from '../../../../../../main/webapp/app/entities/statistic-definition/statistic-definition.service';
import { StatisticDefinition } from '../../../../../../main/webapp/app/entities/statistic-definition/statistic-definition.model';

describe('Component Tests', () => {

    describe('StatisticDefinition Management Detail Component', () => {
        let comp: StatisticDefinitionDetailComponent;
        let fixture: ComponentFixture<StatisticDefinitionDetailComponent>;
        let service: StatisticDefinitionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PopTestModule],
                declarations: [StatisticDefinitionDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    StatisticDefinitionService,
                    JhiEventManager
                ]
            }).overrideTemplate(StatisticDefinitionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(StatisticDefinitionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(StatisticDefinitionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new StatisticDefinition(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.statisticDefinition).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
