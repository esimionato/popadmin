/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PopTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { VirtualCurrencyDetailComponent } from '../../../../../../main/webapp/app/entities/virtual-currency/virtual-currency-detail.component';
import { VirtualCurrencyService } from '../../../../../../main/webapp/app/entities/virtual-currency/virtual-currency.service';
import { VirtualCurrency } from '../../../../../../main/webapp/app/entities/virtual-currency/virtual-currency.model';

describe('Component Tests', () => {

    describe('VirtualCurrency Management Detail Component', () => {
        let comp: VirtualCurrencyDetailComponent;
        let fixture: ComponentFixture<VirtualCurrencyDetailComponent>;
        let service: VirtualCurrencyService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PopTestModule],
                declarations: [VirtualCurrencyDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    VirtualCurrencyService,
                    JhiEventManager
                ]
            }).overrideTemplate(VirtualCurrencyDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VirtualCurrencyDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VirtualCurrencyService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new VirtualCurrency(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.virtualCurrency).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
