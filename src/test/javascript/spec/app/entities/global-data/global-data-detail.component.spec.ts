/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PopTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { GlobalDataDetailComponent } from '../../../../../../main/webapp/app/entities/global-data/global-data-detail.component';
import { GlobalDataService } from '../../../../../../main/webapp/app/entities/global-data/global-data.service';
import { GlobalData } from '../../../../../../main/webapp/app/entities/global-data/global-data.model';

describe('Component Tests', () => {

    describe('GlobalData Management Detail Component', () => {
        let comp: GlobalDataDetailComponent;
        let fixture: ComponentFixture<GlobalDataDetailComponent>;
        let service: GlobalDataService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PopTestModule],
                declarations: [GlobalDataDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    GlobalDataService,
                    JhiEventManager
                ]
            }).overrideTemplate(GlobalDataDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GlobalDataDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GlobalDataService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new GlobalData(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.globalData).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
