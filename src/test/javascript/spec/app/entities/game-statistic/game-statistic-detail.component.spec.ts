/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PopTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { GameStatisticDetailComponent } from '../../../../../../main/webapp/app/entities/game-statistic/game-statistic-detail.component';
import { GameStatisticService } from '../../../../../../main/webapp/app/entities/game-statistic/game-statistic.service';
import { GameStatistic } from '../../../../../../main/webapp/app/entities/game-statistic/game-statistic.model';

describe('Component Tests', () => {

    describe('GameStatistic Management Detail Component', () => {
        let comp: GameStatisticDetailComponent;
        let fixture: ComponentFixture<GameStatisticDetailComponent>;
        let service: GameStatisticService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PopTestModule],
                declarations: [GameStatisticDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    GameStatisticService,
                    JhiEventManager
                ]
            }).overrideTemplate(GameStatisticDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(GameStatisticDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(GameStatisticService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new GameStatistic(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.gameStatistic).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
