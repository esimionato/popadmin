/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { PopTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PlayerDataDetailComponent } from '../../../../../../main/webapp/app/entities/player-data/player-data-detail.component';
import { PlayerDataService } from '../../../../../../main/webapp/app/entities/player-data/player-data.service';
import { PlayerData } from '../../../../../../main/webapp/app/entities/player-data/player-data.model';

describe('Component Tests', () => {

    describe('PlayerData Management Detail Component', () => {
        let comp: PlayerDataDetailComponent;
        let fixture: ComponentFixture<PlayerDataDetailComponent>;
        let service: PlayerDataService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [PopTestModule],
                declarations: [PlayerDataDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PlayerDataService,
                    JhiEventManager
                ]
            }).overrideTemplate(PlayerDataDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PlayerDataDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PlayerDataService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PlayerData(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.playerData).toEqual(jasmine.objectContaining({id: 10}));
            });
        });
    });

});
