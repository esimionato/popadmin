package pop.web.rest;

import pop.PopApp;

import pop.domain.PlayerVcurrencyBalance;
import pop.domain.Player;
import pop.domain.VirtualCurrency;
import pop.repository.PlayerVcurrencyBalanceRepository;
import pop.service.PlayerVcurrencyBalanceService;
import pop.service.dto.PlayerVcurrencyBalanceDTO;
import pop.service.mapper.PlayerVcurrencyBalanceMapper;
import pop.web.rest.errors.ExceptionTranslator;
import pop.service.dto.PlayerVcurrencyBalanceCriteria;
import pop.service.PlayerVcurrencyBalanceQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static pop.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PlayerVcurrencyBalanceResource REST controller.
 *
 * @see PlayerVcurrencyBalanceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PopApp.class)
public class PlayerVcurrencyBalanceResourceIntTest {

    private static final Integer DEFAULT_AMOUNT = 1;
    private static final Integer UPDATED_AMOUNT = 2;

    @Autowired
    private PlayerVcurrencyBalanceRepository playerVcurrencyBalanceRepository;

    @Autowired
    private PlayerVcurrencyBalanceMapper playerVcurrencyBalanceMapper;

    @Autowired
    private PlayerVcurrencyBalanceService playerVcurrencyBalanceService;

    @Autowired
    private PlayerVcurrencyBalanceQueryService playerVcurrencyBalanceQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPlayerVcurrencyBalanceMockMvc;

    private PlayerVcurrencyBalance playerVcurrencyBalance;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PlayerVcurrencyBalanceResource playerVcurrencyBalanceResource = new PlayerVcurrencyBalanceResource(playerVcurrencyBalanceService, playerVcurrencyBalanceQueryService);
        this.restPlayerVcurrencyBalanceMockMvc = MockMvcBuilders.standaloneSetup(playerVcurrencyBalanceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PlayerVcurrencyBalance createEntity(EntityManager em) {
        PlayerVcurrencyBalance playerVcurrencyBalance = new PlayerVcurrencyBalance()
            .amount(DEFAULT_AMOUNT);
        return playerVcurrencyBalance;
    }

    @Before
    public void initTest() {
        playerVcurrencyBalance = createEntity(em);
    }

    @Test
    @Transactional
    public void createPlayerVcurrencyBalance() throws Exception {
        int databaseSizeBeforeCreate = playerVcurrencyBalanceRepository.findAll().size();

        // Create the PlayerVcurrencyBalance
        PlayerVcurrencyBalanceDTO playerVcurrencyBalanceDTO = playerVcurrencyBalanceMapper.toDto(playerVcurrencyBalance);
        restPlayerVcurrencyBalanceMockMvc.perform(post("/api/player-vcurrency-balances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(playerVcurrencyBalanceDTO)))
            .andExpect(status().isCreated());

        // Validate the PlayerVcurrencyBalance in the database
        List<PlayerVcurrencyBalance> playerVcurrencyBalanceList = playerVcurrencyBalanceRepository.findAll();
        assertThat(playerVcurrencyBalanceList).hasSize(databaseSizeBeforeCreate + 1);
        PlayerVcurrencyBalance testPlayerVcurrencyBalance = playerVcurrencyBalanceList.get(playerVcurrencyBalanceList.size() - 1);
        assertThat(testPlayerVcurrencyBalance.getAmount()).isEqualTo(DEFAULT_AMOUNT);
    }

    @Test
    @Transactional
    public void createPlayerVcurrencyBalanceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = playerVcurrencyBalanceRepository.findAll().size();

        // Create the PlayerVcurrencyBalance with an existing ID
        playerVcurrencyBalance.setId(1L);
        PlayerVcurrencyBalanceDTO playerVcurrencyBalanceDTO = playerVcurrencyBalanceMapper.toDto(playerVcurrencyBalance);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPlayerVcurrencyBalanceMockMvc.perform(post("/api/player-vcurrency-balances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(playerVcurrencyBalanceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PlayerVcurrencyBalance in the database
        List<PlayerVcurrencyBalance> playerVcurrencyBalanceList = playerVcurrencyBalanceRepository.findAll();
        assertThat(playerVcurrencyBalanceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPlayerVcurrencyBalances() throws Exception {
        // Initialize the database
        playerVcurrencyBalanceRepository.saveAndFlush(playerVcurrencyBalance);

        // Get all the playerVcurrencyBalanceList
        restPlayerVcurrencyBalanceMockMvc.perform(get("/api/player-vcurrency-balances?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(playerVcurrencyBalance.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)));
    }

    @Test
    @Transactional
    public void getPlayerVcurrencyBalance() throws Exception {
        // Initialize the database
        playerVcurrencyBalanceRepository.saveAndFlush(playerVcurrencyBalance);

        // Get the playerVcurrencyBalance
        restPlayerVcurrencyBalanceMockMvc.perform(get("/api/player-vcurrency-balances/{id}", playerVcurrencyBalance.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(playerVcurrencyBalance.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT));
    }

    @Test
    @Transactional
    public void getAllPlayerVcurrencyBalancesByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        playerVcurrencyBalanceRepository.saveAndFlush(playerVcurrencyBalance);

        // Get all the playerVcurrencyBalanceList where amount equals to DEFAULT_AMOUNT
        defaultPlayerVcurrencyBalanceShouldBeFound("amount.equals=" + DEFAULT_AMOUNT);

        // Get all the playerVcurrencyBalanceList where amount equals to UPDATED_AMOUNT
        defaultPlayerVcurrencyBalanceShouldNotBeFound("amount.equals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllPlayerVcurrencyBalancesByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        playerVcurrencyBalanceRepository.saveAndFlush(playerVcurrencyBalance);

        // Get all the playerVcurrencyBalanceList where amount in DEFAULT_AMOUNT or UPDATED_AMOUNT
        defaultPlayerVcurrencyBalanceShouldBeFound("amount.in=" + DEFAULT_AMOUNT + "," + UPDATED_AMOUNT);

        // Get all the playerVcurrencyBalanceList where amount equals to UPDATED_AMOUNT
        defaultPlayerVcurrencyBalanceShouldNotBeFound("amount.in=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllPlayerVcurrencyBalancesByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        playerVcurrencyBalanceRepository.saveAndFlush(playerVcurrencyBalance);

        // Get all the playerVcurrencyBalanceList where amount is not null
        defaultPlayerVcurrencyBalanceShouldBeFound("amount.specified=true");

        // Get all the playerVcurrencyBalanceList where amount is null
        defaultPlayerVcurrencyBalanceShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    public void getAllPlayerVcurrencyBalancesByAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        playerVcurrencyBalanceRepository.saveAndFlush(playerVcurrencyBalance);

        // Get all the playerVcurrencyBalanceList where amount greater than or equals to DEFAULT_AMOUNT
        defaultPlayerVcurrencyBalanceShouldBeFound("amount.greaterOrEqualThan=" + DEFAULT_AMOUNT);

        // Get all the playerVcurrencyBalanceList where amount greater than or equals to UPDATED_AMOUNT
        defaultPlayerVcurrencyBalanceShouldNotBeFound("amount.greaterOrEqualThan=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void getAllPlayerVcurrencyBalancesByAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        playerVcurrencyBalanceRepository.saveAndFlush(playerVcurrencyBalance);

        // Get all the playerVcurrencyBalanceList where amount less than or equals to DEFAULT_AMOUNT
        defaultPlayerVcurrencyBalanceShouldNotBeFound("amount.lessThan=" + DEFAULT_AMOUNT);

        // Get all the playerVcurrencyBalanceList where amount less than or equals to UPDATED_AMOUNT
        defaultPlayerVcurrencyBalanceShouldBeFound("amount.lessThan=" + UPDATED_AMOUNT);
    }


    @Test
    @Transactional
    public void getAllPlayerVcurrencyBalancesByPlayerIsEqualToSomething() throws Exception {
        // Initialize the database
        Player player = PlayerResourceIntTest.createEntity(em);
        em.persist(player);
        em.flush();
        playerVcurrencyBalance.setPlayer(player);
        playerVcurrencyBalanceRepository.saveAndFlush(playerVcurrencyBalance);
        Long playerId = player.getId();

        // Get all the playerVcurrencyBalanceList where player equals to playerId
        defaultPlayerVcurrencyBalanceShouldBeFound("playerId.equals=" + playerId);

        // Get all the playerVcurrencyBalanceList where player equals to playerId + 1
        defaultPlayerVcurrencyBalanceShouldNotBeFound("playerId.equals=" + (playerId + 1));
    }


    @Test
    @Transactional
    public void getAllPlayerVcurrencyBalancesByVirtual_currencyIsEqualToSomething() throws Exception {
        // Initialize the database
        VirtualCurrency virtual_currency = VirtualCurrencyResourceIntTest.createEntity(em);
        em.persist(virtual_currency);
        em.flush();
        playerVcurrencyBalance.setVirtual_currency(virtual_currency);
        playerVcurrencyBalanceRepository.saveAndFlush(playerVcurrencyBalance);
        Long virtual_currencyId = virtual_currency.getId();

        // Get all the playerVcurrencyBalanceList where virtual_currency equals to virtual_currencyId
        defaultPlayerVcurrencyBalanceShouldBeFound("virtual_currencyId.equals=" + virtual_currencyId);

        // Get all the playerVcurrencyBalanceList where virtual_currency equals to virtual_currencyId + 1
        defaultPlayerVcurrencyBalanceShouldNotBeFound("virtual_currencyId.equals=" + (virtual_currencyId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultPlayerVcurrencyBalanceShouldBeFound(String filter) throws Exception {
        restPlayerVcurrencyBalanceMockMvc.perform(get("/api/player-vcurrency-balances?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(playerVcurrencyBalance.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultPlayerVcurrencyBalanceShouldNotBeFound(String filter) throws Exception {
        restPlayerVcurrencyBalanceMockMvc.perform(get("/api/player-vcurrency-balances?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingPlayerVcurrencyBalance() throws Exception {
        // Get the playerVcurrencyBalance
        restPlayerVcurrencyBalanceMockMvc.perform(get("/api/player-vcurrency-balances/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePlayerVcurrencyBalance() throws Exception {
        // Initialize the database
        playerVcurrencyBalanceRepository.saveAndFlush(playerVcurrencyBalance);
        int databaseSizeBeforeUpdate = playerVcurrencyBalanceRepository.findAll().size();

        // Update the playerVcurrencyBalance
        PlayerVcurrencyBalance updatedPlayerVcurrencyBalance = playerVcurrencyBalanceRepository.findOne(playerVcurrencyBalance.getId());
        updatedPlayerVcurrencyBalance
            .amount(UPDATED_AMOUNT);
        PlayerVcurrencyBalanceDTO playerVcurrencyBalanceDTO = playerVcurrencyBalanceMapper.toDto(updatedPlayerVcurrencyBalance);

        restPlayerVcurrencyBalanceMockMvc.perform(put("/api/player-vcurrency-balances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(playerVcurrencyBalanceDTO)))
            .andExpect(status().isOk());

        // Validate the PlayerVcurrencyBalance in the database
        List<PlayerVcurrencyBalance> playerVcurrencyBalanceList = playerVcurrencyBalanceRepository.findAll();
        assertThat(playerVcurrencyBalanceList).hasSize(databaseSizeBeforeUpdate);
        PlayerVcurrencyBalance testPlayerVcurrencyBalance = playerVcurrencyBalanceList.get(playerVcurrencyBalanceList.size() - 1);
        assertThat(testPlayerVcurrencyBalance.getAmount()).isEqualTo(UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void updateNonExistingPlayerVcurrencyBalance() throws Exception {
        int databaseSizeBeforeUpdate = playerVcurrencyBalanceRepository.findAll().size();

        // Create the PlayerVcurrencyBalance
        PlayerVcurrencyBalanceDTO playerVcurrencyBalanceDTO = playerVcurrencyBalanceMapper.toDto(playerVcurrencyBalance);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPlayerVcurrencyBalanceMockMvc.perform(put("/api/player-vcurrency-balances")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(playerVcurrencyBalanceDTO)))
            .andExpect(status().isCreated());

        // Validate the PlayerVcurrencyBalance in the database
        List<PlayerVcurrencyBalance> playerVcurrencyBalanceList = playerVcurrencyBalanceRepository.findAll();
        assertThat(playerVcurrencyBalanceList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePlayerVcurrencyBalance() throws Exception {
        // Initialize the database
        playerVcurrencyBalanceRepository.saveAndFlush(playerVcurrencyBalance);
        int databaseSizeBeforeDelete = playerVcurrencyBalanceRepository.findAll().size();

        // Get the playerVcurrencyBalance
        restPlayerVcurrencyBalanceMockMvc.perform(delete("/api/player-vcurrency-balances/{id}", playerVcurrencyBalance.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PlayerVcurrencyBalance> playerVcurrencyBalanceList = playerVcurrencyBalanceRepository.findAll();
        assertThat(playerVcurrencyBalanceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlayerVcurrencyBalance.class);
        PlayerVcurrencyBalance playerVcurrencyBalance1 = new PlayerVcurrencyBalance();
        playerVcurrencyBalance1.setId(1L);
        PlayerVcurrencyBalance playerVcurrencyBalance2 = new PlayerVcurrencyBalance();
        playerVcurrencyBalance2.setId(playerVcurrencyBalance1.getId());
        assertThat(playerVcurrencyBalance1).isEqualTo(playerVcurrencyBalance2);
        playerVcurrencyBalance2.setId(2L);
        assertThat(playerVcurrencyBalance1).isNotEqualTo(playerVcurrencyBalance2);
        playerVcurrencyBalance1.setId(null);
        assertThat(playerVcurrencyBalance1).isNotEqualTo(playerVcurrencyBalance2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PlayerVcurrencyBalanceDTO.class);
        PlayerVcurrencyBalanceDTO playerVcurrencyBalanceDTO1 = new PlayerVcurrencyBalanceDTO();
        playerVcurrencyBalanceDTO1.setId(1L);
        PlayerVcurrencyBalanceDTO playerVcurrencyBalanceDTO2 = new PlayerVcurrencyBalanceDTO();
        assertThat(playerVcurrencyBalanceDTO1).isNotEqualTo(playerVcurrencyBalanceDTO2);
        playerVcurrencyBalanceDTO2.setId(playerVcurrencyBalanceDTO1.getId());
        assertThat(playerVcurrencyBalanceDTO1).isEqualTo(playerVcurrencyBalanceDTO2);
        playerVcurrencyBalanceDTO2.setId(2L);
        assertThat(playerVcurrencyBalanceDTO1).isNotEqualTo(playerVcurrencyBalanceDTO2);
        playerVcurrencyBalanceDTO1.setId(null);
        assertThat(playerVcurrencyBalanceDTO1).isNotEqualTo(playerVcurrencyBalanceDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(playerVcurrencyBalanceMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(playerVcurrencyBalanceMapper.fromId(null)).isNull();
    }
}
