package pop.web.rest;

import pop.PopApp;

import pop.domain.VirtualCurrency;
import pop.domain.Game;
import pop.repository.VirtualCurrencyRepository;
import pop.service.VirtualCurrencyService;
import pop.service.dto.VirtualCurrencyDTO;
import pop.service.mapper.VirtualCurrencyMapper;
import pop.web.rest.errors.ExceptionTranslator;
import pop.service.dto.VirtualCurrencyCriteria;
import pop.service.VirtualCurrencyQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static pop.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VirtualCurrencyResource REST controller.
 *
 * @see VirtualCurrencyResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PopApp.class)
public class VirtualCurrencyResourceIntTest {

    private static final String DEFAULT_CURRENCY_ICON_URL = "AAAAAAAAAA";
    private static final String UPDATED_CURRENCY_ICON_URL = "BBBBBBBBBB";

    private static final String DEFAULT_CURRENCY_CODE = "AA";
    private static final String UPDATED_CURRENCY_CODE = "BB";

    private static final String DEFAULT_CURRENCY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CURRENCY_NAME = "BBBBBBBBBB";

    private static final Integer DEFAULT_INITIAL_DEPOSIT = 1;
    private static final Integer UPDATED_INITIAL_DEPOSIT = 2;

    private static final Integer DEFAULT_RECHARGE_RATE = 1;
    private static final Integer UPDATED_RECHARGE_RATE = 2;

    private static final Integer DEFAULT_MAX_LIMIT = 1;
    private static final Integer UPDATED_MAX_LIMIT = 2;

    private static final Integer DEFAULT_CURRENCY_RATIO = 1;
    private static final Integer UPDATED_CURRENCY_RATIO = 2;

    private static final Boolean DEFAULT_CURRENCY_BASE = false;
    private static final Boolean UPDATED_CURRENCY_BASE = true;

    @Autowired
    private VirtualCurrencyRepository virtualCurrencyRepository;

    @Autowired
    private VirtualCurrencyMapper virtualCurrencyMapper;

    @Autowired
    private VirtualCurrencyService virtualCurrencyService;

    @Autowired
    private VirtualCurrencyQueryService virtualCurrencyQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restVirtualCurrencyMockMvc;

    private VirtualCurrency virtualCurrency;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VirtualCurrencyResource virtualCurrencyResource = new VirtualCurrencyResource(virtualCurrencyService, virtualCurrencyQueryService);
        this.restVirtualCurrencyMockMvc = MockMvcBuilders.standaloneSetup(virtualCurrencyResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VirtualCurrency createEntity(EntityManager em) {
        VirtualCurrency virtualCurrency = new VirtualCurrency()
            .currencyIconUrl(DEFAULT_CURRENCY_ICON_URL)
            .currencyCode(DEFAULT_CURRENCY_CODE)
            .currencyName(DEFAULT_CURRENCY_NAME)
            .initialDeposit(DEFAULT_INITIAL_DEPOSIT)
            .rechargeRate(DEFAULT_RECHARGE_RATE)
            .maxLimit(DEFAULT_MAX_LIMIT)
            .currencyRatio(DEFAULT_CURRENCY_RATIO)
            .currencyBase(DEFAULT_CURRENCY_BASE);
        return virtualCurrency;
    }

    @Before
    public void initTest() {
        virtualCurrency = createEntity(em);
    }

    @Test
    @Transactional
    public void createVirtualCurrency() throws Exception {
        int databaseSizeBeforeCreate = virtualCurrencyRepository.findAll().size();

        // Create the VirtualCurrency
        VirtualCurrencyDTO virtualCurrencyDTO = virtualCurrencyMapper.toDto(virtualCurrency);
        restVirtualCurrencyMockMvc.perform(post("/api/virtual-currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(virtualCurrencyDTO)))
            .andExpect(status().isCreated());

        // Validate the VirtualCurrency in the database
        List<VirtualCurrency> virtualCurrencyList = virtualCurrencyRepository.findAll();
        assertThat(virtualCurrencyList).hasSize(databaseSizeBeforeCreate + 1);
        VirtualCurrency testVirtualCurrency = virtualCurrencyList.get(virtualCurrencyList.size() - 1);
        assertThat(testVirtualCurrency.getCurrencyIconUrl()).isEqualTo(DEFAULT_CURRENCY_ICON_URL);
        assertThat(testVirtualCurrency.getCurrencyCode()).isEqualTo(DEFAULT_CURRENCY_CODE);
        assertThat(testVirtualCurrency.getCurrencyName()).isEqualTo(DEFAULT_CURRENCY_NAME);
        assertThat(testVirtualCurrency.getInitialDeposit()).isEqualTo(DEFAULT_INITIAL_DEPOSIT);
        assertThat(testVirtualCurrency.getRechargeRate()).isEqualTo(DEFAULT_RECHARGE_RATE);
        assertThat(testVirtualCurrency.getMaxLimit()).isEqualTo(DEFAULT_MAX_LIMIT);
        assertThat(testVirtualCurrency.getCurrencyRatio()).isEqualTo(DEFAULT_CURRENCY_RATIO);
        assertThat(testVirtualCurrency.isCurrencyBase()).isEqualTo(DEFAULT_CURRENCY_BASE);
    }

    @Test
    @Transactional
    public void createVirtualCurrencyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = virtualCurrencyRepository.findAll().size();

        // Create the VirtualCurrency with an existing ID
        virtualCurrency.setId(1L);
        VirtualCurrencyDTO virtualCurrencyDTO = virtualCurrencyMapper.toDto(virtualCurrency);

        // An entity with an existing ID cannot be created, so this API call must fail
        restVirtualCurrencyMockMvc.perform(post("/api/virtual-currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(virtualCurrencyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the VirtualCurrency in the database
        List<VirtualCurrency> virtualCurrencyList = virtualCurrencyRepository.findAll();
        assertThat(virtualCurrencyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCurrencyCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = virtualCurrencyRepository.findAll().size();
        // set the field null
        virtualCurrency.setCurrencyCode(null);

        // Create the VirtualCurrency, which fails.
        VirtualCurrencyDTO virtualCurrencyDTO = virtualCurrencyMapper.toDto(virtualCurrency);

        restVirtualCurrencyMockMvc.perform(post("/api/virtual-currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(virtualCurrencyDTO)))
            .andExpect(status().isBadRequest());

        List<VirtualCurrency> virtualCurrencyList = virtualCurrencyRepository.findAll();
        assertThat(virtualCurrencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCurrencyNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = virtualCurrencyRepository.findAll().size();
        // set the field null
        virtualCurrency.setCurrencyName(null);

        // Create the VirtualCurrency, which fails.
        VirtualCurrencyDTO virtualCurrencyDTO = virtualCurrencyMapper.toDto(virtualCurrency);

        restVirtualCurrencyMockMvc.perform(post("/api/virtual-currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(virtualCurrencyDTO)))
            .andExpect(status().isBadRequest());

        List<VirtualCurrency> virtualCurrencyList = virtualCurrencyRepository.findAll();
        assertThat(virtualCurrencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrencies() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList
        restVirtualCurrencyMockMvc.perform(get("/api/virtual-currencies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(virtualCurrency.getId().intValue())))
            .andExpect(jsonPath("$.[*].currencyIconUrl").value(hasItem(DEFAULT_CURRENCY_ICON_URL.toString())))
            .andExpect(jsonPath("$.[*].currencyCode").value(hasItem(DEFAULT_CURRENCY_CODE.toString())))
            .andExpect(jsonPath("$.[*].currencyName").value(hasItem(DEFAULT_CURRENCY_NAME.toString())))
            .andExpect(jsonPath("$.[*].initialDeposit").value(hasItem(DEFAULT_INITIAL_DEPOSIT)))
            .andExpect(jsonPath("$.[*].rechargeRate").value(hasItem(DEFAULT_RECHARGE_RATE)))
            .andExpect(jsonPath("$.[*].maxLimit").value(hasItem(DEFAULT_MAX_LIMIT)))
            .andExpect(jsonPath("$.[*].currencyRatio").value(hasItem(DEFAULT_CURRENCY_RATIO)))
            .andExpect(jsonPath("$.[*].currencyBase").value(hasItem(DEFAULT_CURRENCY_BASE.booleanValue())));
    }

    @Test
    @Transactional
    public void getVirtualCurrency() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get the virtualCurrency
        restVirtualCurrencyMockMvc.perform(get("/api/virtual-currencies/{id}", virtualCurrency.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(virtualCurrency.getId().intValue()))
            .andExpect(jsonPath("$.currencyIconUrl").value(DEFAULT_CURRENCY_ICON_URL.toString()))
            .andExpect(jsonPath("$.currencyCode").value(DEFAULT_CURRENCY_CODE.toString()))
            .andExpect(jsonPath("$.currencyName").value(DEFAULT_CURRENCY_NAME.toString()))
            .andExpect(jsonPath("$.initialDeposit").value(DEFAULT_INITIAL_DEPOSIT))
            .andExpect(jsonPath("$.rechargeRate").value(DEFAULT_RECHARGE_RATE))
            .andExpect(jsonPath("$.maxLimit").value(DEFAULT_MAX_LIMIT))
            .andExpect(jsonPath("$.currencyRatio").value(DEFAULT_CURRENCY_RATIO))
            .andExpect(jsonPath("$.currencyBase").value(DEFAULT_CURRENCY_BASE.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyIconUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyIconUrl equals to DEFAULT_CURRENCY_ICON_URL
        defaultVirtualCurrencyShouldBeFound("currencyIconUrl.equals=" + DEFAULT_CURRENCY_ICON_URL);

        // Get all the virtualCurrencyList where currencyIconUrl equals to UPDATED_CURRENCY_ICON_URL
        defaultVirtualCurrencyShouldNotBeFound("currencyIconUrl.equals=" + UPDATED_CURRENCY_ICON_URL);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyIconUrlIsInShouldWork() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyIconUrl in DEFAULT_CURRENCY_ICON_URL or UPDATED_CURRENCY_ICON_URL
        defaultVirtualCurrencyShouldBeFound("currencyIconUrl.in=" + DEFAULT_CURRENCY_ICON_URL + "," + UPDATED_CURRENCY_ICON_URL);

        // Get all the virtualCurrencyList where currencyIconUrl equals to UPDATED_CURRENCY_ICON_URL
        defaultVirtualCurrencyShouldNotBeFound("currencyIconUrl.in=" + UPDATED_CURRENCY_ICON_URL);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyIconUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyIconUrl is not null
        defaultVirtualCurrencyShouldBeFound("currencyIconUrl.specified=true");

        // Get all the virtualCurrencyList where currencyIconUrl is null
        defaultVirtualCurrencyShouldNotBeFound("currencyIconUrl.specified=false");
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyCode equals to DEFAULT_CURRENCY_CODE
        defaultVirtualCurrencyShouldBeFound("currencyCode.equals=" + DEFAULT_CURRENCY_CODE);

        // Get all the virtualCurrencyList where currencyCode equals to UPDATED_CURRENCY_CODE
        defaultVirtualCurrencyShouldNotBeFound("currencyCode.equals=" + UPDATED_CURRENCY_CODE);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyCodeIsInShouldWork() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyCode in DEFAULT_CURRENCY_CODE or UPDATED_CURRENCY_CODE
        defaultVirtualCurrencyShouldBeFound("currencyCode.in=" + DEFAULT_CURRENCY_CODE + "," + UPDATED_CURRENCY_CODE);

        // Get all the virtualCurrencyList where currencyCode equals to UPDATED_CURRENCY_CODE
        defaultVirtualCurrencyShouldNotBeFound("currencyCode.in=" + UPDATED_CURRENCY_CODE);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyCode is not null
        defaultVirtualCurrencyShouldBeFound("currencyCode.specified=true");

        // Get all the virtualCurrencyList where currencyCode is null
        defaultVirtualCurrencyShouldNotBeFound("currencyCode.specified=false");
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyNameIsEqualToSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyName equals to DEFAULT_CURRENCY_NAME
        defaultVirtualCurrencyShouldBeFound("currencyName.equals=" + DEFAULT_CURRENCY_NAME);

        // Get all the virtualCurrencyList where currencyName equals to UPDATED_CURRENCY_NAME
        defaultVirtualCurrencyShouldNotBeFound("currencyName.equals=" + UPDATED_CURRENCY_NAME);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyNameIsInShouldWork() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyName in DEFAULT_CURRENCY_NAME or UPDATED_CURRENCY_NAME
        defaultVirtualCurrencyShouldBeFound("currencyName.in=" + DEFAULT_CURRENCY_NAME + "," + UPDATED_CURRENCY_NAME);

        // Get all the virtualCurrencyList where currencyName equals to UPDATED_CURRENCY_NAME
        defaultVirtualCurrencyShouldNotBeFound("currencyName.in=" + UPDATED_CURRENCY_NAME);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyName is not null
        defaultVirtualCurrencyShouldBeFound("currencyName.specified=true");

        // Get all the virtualCurrencyList where currencyName is null
        defaultVirtualCurrencyShouldNotBeFound("currencyName.specified=false");
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByInitialDepositIsEqualToSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where initialDeposit equals to DEFAULT_INITIAL_DEPOSIT
        defaultVirtualCurrencyShouldBeFound("initialDeposit.equals=" + DEFAULT_INITIAL_DEPOSIT);

        // Get all the virtualCurrencyList where initialDeposit equals to UPDATED_INITIAL_DEPOSIT
        defaultVirtualCurrencyShouldNotBeFound("initialDeposit.equals=" + UPDATED_INITIAL_DEPOSIT);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByInitialDepositIsInShouldWork() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where initialDeposit in DEFAULT_INITIAL_DEPOSIT or UPDATED_INITIAL_DEPOSIT
        defaultVirtualCurrencyShouldBeFound("initialDeposit.in=" + DEFAULT_INITIAL_DEPOSIT + "," + UPDATED_INITIAL_DEPOSIT);

        // Get all the virtualCurrencyList where initialDeposit equals to UPDATED_INITIAL_DEPOSIT
        defaultVirtualCurrencyShouldNotBeFound("initialDeposit.in=" + UPDATED_INITIAL_DEPOSIT);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByInitialDepositIsNullOrNotNull() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where initialDeposit is not null
        defaultVirtualCurrencyShouldBeFound("initialDeposit.specified=true");

        // Get all the virtualCurrencyList where initialDeposit is null
        defaultVirtualCurrencyShouldNotBeFound("initialDeposit.specified=false");
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByInitialDepositIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where initialDeposit greater than or equals to DEFAULT_INITIAL_DEPOSIT
        defaultVirtualCurrencyShouldBeFound("initialDeposit.greaterOrEqualThan=" + DEFAULT_INITIAL_DEPOSIT);

        // Get all the virtualCurrencyList where initialDeposit greater than or equals to UPDATED_INITIAL_DEPOSIT
        defaultVirtualCurrencyShouldNotBeFound("initialDeposit.greaterOrEqualThan=" + UPDATED_INITIAL_DEPOSIT);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByInitialDepositIsLessThanSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where initialDeposit less than or equals to DEFAULT_INITIAL_DEPOSIT
        defaultVirtualCurrencyShouldNotBeFound("initialDeposit.lessThan=" + DEFAULT_INITIAL_DEPOSIT);

        // Get all the virtualCurrencyList where initialDeposit less than or equals to UPDATED_INITIAL_DEPOSIT
        defaultVirtualCurrencyShouldBeFound("initialDeposit.lessThan=" + UPDATED_INITIAL_DEPOSIT);
    }


    @Test
    @Transactional
    public void getAllVirtualCurrenciesByRechargeRateIsEqualToSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where rechargeRate equals to DEFAULT_RECHARGE_RATE
        defaultVirtualCurrencyShouldBeFound("rechargeRate.equals=" + DEFAULT_RECHARGE_RATE);

        // Get all the virtualCurrencyList where rechargeRate equals to UPDATED_RECHARGE_RATE
        defaultVirtualCurrencyShouldNotBeFound("rechargeRate.equals=" + UPDATED_RECHARGE_RATE);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByRechargeRateIsInShouldWork() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where rechargeRate in DEFAULT_RECHARGE_RATE or UPDATED_RECHARGE_RATE
        defaultVirtualCurrencyShouldBeFound("rechargeRate.in=" + DEFAULT_RECHARGE_RATE + "," + UPDATED_RECHARGE_RATE);

        // Get all the virtualCurrencyList where rechargeRate equals to UPDATED_RECHARGE_RATE
        defaultVirtualCurrencyShouldNotBeFound("rechargeRate.in=" + UPDATED_RECHARGE_RATE);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByRechargeRateIsNullOrNotNull() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where rechargeRate is not null
        defaultVirtualCurrencyShouldBeFound("rechargeRate.specified=true");

        // Get all the virtualCurrencyList where rechargeRate is null
        defaultVirtualCurrencyShouldNotBeFound("rechargeRate.specified=false");
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByRechargeRateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where rechargeRate greater than or equals to DEFAULT_RECHARGE_RATE
        defaultVirtualCurrencyShouldBeFound("rechargeRate.greaterOrEqualThan=" + DEFAULT_RECHARGE_RATE);

        // Get all the virtualCurrencyList where rechargeRate greater than or equals to UPDATED_RECHARGE_RATE
        defaultVirtualCurrencyShouldNotBeFound("rechargeRate.greaterOrEqualThan=" + UPDATED_RECHARGE_RATE);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByRechargeRateIsLessThanSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where rechargeRate less than or equals to DEFAULT_RECHARGE_RATE
        defaultVirtualCurrencyShouldNotBeFound("rechargeRate.lessThan=" + DEFAULT_RECHARGE_RATE);

        // Get all the virtualCurrencyList where rechargeRate less than or equals to UPDATED_RECHARGE_RATE
        defaultVirtualCurrencyShouldBeFound("rechargeRate.lessThan=" + UPDATED_RECHARGE_RATE);
    }


    @Test
    @Transactional
    public void getAllVirtualCurrenciesByMaxLimitIsEqualToSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where maxLimit equals to DEFAULT_MAX_LIMIT
        defaultVirtualCurrencyShouldBeFound("maxLimit.equals=" + DEFAULT_MAX_LIMIT);

        // Get all the virtualCurrencyList where maxLimit equals to UPDATED_MAX_LIMIT
        defaultVirtualCurrencyShouldNotBeFound("maxLimit.equals=" + UPDATED_MAX_LIMIT);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByMaxLimitIsInShouldWork() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where maxLimit in DEFAULT_MAX_LIMIT or UPDATED_MAX_LIMIT
        defaultVirtualCurrencyShouldBeFound("maxLimit.in=" + DEFAULT_MAX_LIMIT + "," + UPDATED_MAX_LIMIT);

        // Get all the virtualCurrencyList where maxLimit equals to UPDATED_MAX_LIMIT
        defaultVirtualCurrencyShouldNotBeFound("maxLimit.in=" + UPDATED_MAX_LIMIT);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByMaxLimitIsNullOrNotNull() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where maxLimit is not null
        defaultVirtualCurrencyShouldBeFound("maxLimit.specified=true");

        // Get all the virtualCurrencyList where maxLimit is null
        defaultVirtualCurrencyShouldNotBeFound("maxLimit.specified=false");
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByMaxLimitIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where maxLimit greater than or equals to DEFAULT_MAX_LIMIT
        defaultVirtualCurrencyShouldBeFound("maxLimit.greaterOrEqualThan=" + DEFAULT_MAX_LIMIT);

        // Get all the virtualCurrencyList where maxLimit greater than or equals to UPDATED_MAX_LIMIT
        defaultVirtualCurrencyShouldNotBeFound("maxLimit.greaterOrEqualThan=" + UPDATED_MAX_LIMIT);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByMaxLimitIsLessThanSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where maxLimit less than or equals to DEFAULT_MAX_LIMIT
        defaultVirtualCurrencyShouldNotBeFound("maxLimit.lessThan=" + DEFAULT_MAX_LIMIT);

        // Get all the virtualCurrencyList where maxLimit less than or equals to UPDATED_MAX_LIMIT
        defaultVirtualCurrencyShouldBeFound("maxLimit.lessThan=" + UPDATED_MAX_LIMIT);
    }


    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyRatioIsEqualToSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyRatio equals to DEFAULT_CURRENCY_RATIO
        defaultVirtualCurrencyShouldBeFound("currencyRatio.equals=" + DEFAULT_CURRENCY_RATIO);

        // Get all the virtualCurrencyList where currencyRatio equals to UPDATED_CURRENCY_RATIO
        defaultVirtualCurrencyShouldNotBeFound("currencyRatio.equals=" + UPDATED_CURRENCY_RATIO);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyRatioIsInShouldWork() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyRatio in DEFAULT_CURRENCY_RATIO or UPDATED_CURRENCY_RATIO
        defaultVirtualCurrencyShouldBeFound("currencyRatio.in=" + DEFAULT_CURRENCY_RATIO + "," + UPDATED_CURRENCY_RATIO);

        // Get all the virtualCurrencyList where currencyRatio equals to UPDATED_CURRENCY_RATIO
        defaultVirtualCurrencyShouldNotBeFound("currencyRatio.in=" + UPDATED_CURRENCY_RATIO);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyRatioIsNullOrNotNull() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyRatio is not null
        defaultVirtualCurrencyShouldBeFound("currencyRatio.specified=true");

        // Get all the virtualCurrencyList where currencyRatio is null
        defaultVirtualCurrencyShouldNotBeFound("currencyRatio.specified=false");
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyRatioIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyRatio greater than or equals to DEFAULT_CURRENCY_RATIO
        defaultVirtualCurrencyShouldBeFound("currencyRatio.greaterOrEqualThan=" + DEFAULT_CURRENCY_RATIO);

        // Get all the virtualCurrencyList where currencyRatio greater than or equals to UPDATED_CURRENCY_RATIO
        defaultVirtualCurrencyShouldNotBeFound("currencyRatio.greaterOrEqualThan=" + UPDATED_CURRENCY_RATIO);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyRatioIsLessThanSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyRatio less than or equals to DEFAULT_CURRENCY_RATIO
        defaultVirtualCurrencyShouldNotBeFound("currencyRatio.lessThan=" + DEFAULT_CURRENCY_RATIO);

        // Get all the virtualCurrencyList where currencyRatio less than or equals to UPDATED_CURRENCY_RATIO
        defaultVirtualCurrencyShouldBeFound("currencyRatio.lessThan=" + UPDATED_CURRENCY_RATIO);
    }


    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyBaseIsEqualToSomething() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyBase equals to DEFAULT_CURRENCY_BASE
        defaultVirtualCurrencyShouldBeFound("currencyBase.equals=" + DEFAULT_CURRENCY_BASE);

        // Get all the virtualCurrencyList where currencyBase equals to UPDATED_CURRENCY_BASE
        defaultVirtualCurrencyShouldNotBeFound("currencyBase.equals=" + UPDATED_CURRENCY_BASE);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyBaseIsInShouldWork() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyBase in DEFAULT_CURRENCY_BASE or UPDATED_CURRENCY_BASE
        defaultVirtualCurrencyShouldBeFound("currencyBase.in=" + DEFAULT_CURRENCY_BASE + "," + UPDATED_CURRENCY_BASE);

        // Get all the virtualCurrencyList where currencyBase equals to UPDATED_CURRENCY_BASE
        defaultVirtualCurrencyShouldNotBeFound("currencyBase.in=" + UPDATED_CURRENCY_BASE);
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByCurrencyBaseIsNullOrNotNull() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);

        // Get all the virtualCurrencyList where currencyBase is not null
        defaultVirtualCurrencyShouldBeFound("currencyBase.specified=true");

        // Get all the virtualCurrencyList where currencyBase is null
        defaultVirtualCurrencyShouldNotBeFound("currencyBase.specified=false");
    }

    @Test
    @Transactional
    public void getAllVirtualCurrenciesByGameIsEqualToSomething() throws Exception {
        // Initialize the database
        Game game = GameResourceIntTest.createEntity(em);
        em.persist(game);
        em.flush();
        virtualCurrency.setGame(game);
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);
        Long gameId = game.getId();

        // Get all the virtualCurrencyList where game equals to gameId
        defaultVirtualCurrencyShouldBeFound("gameId.equals=" + gameId);

        // Get all the virtualCurrencyList where game equals to gameId + 1
        defaultVirtualCurrencyShouldNotBeFound("gameId.equals=" + (gameId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultVirtualCurrencyShouldBeFound(String filter) throws Exception {
        restVirtualCurrencyMockMvc.perform(get("/api/virtual-currencies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(virtualCurrency.getId().intValue())))
            .andExpect(jsonPath("$.[*].currencyIconUrl").value(hasItem(DEFAULT_CURRENCY_ICON_URL.toString())))
            .andExpect(jsonPath("$.[*].currencyCode").value(hasItem(DEFAULT_CURRENCY_CODE.toString())))
            .andExpect(jsonPath("$.[*].currencyName").value(hasItem(DEFAULT_CURRENCY_NAME.toString())))
            .andExpect(jsonPath("$.[*].initialDeposit").value(hasItem(DEFAULT_INITIAL_DEPOSIT)))
            .andExpect(jsonPath("$.[*].rechargeRate").value(hasItem(DEFAULT_RECHARGE_RATE)))
            .andExpect(jsonPath("$.[*].maxLimit").value(hasItem(DEFAULT_MAX_LIMIT)))
            .andExpect(jsonPath("$.[*].currencyRatio").value(hasItem(DEFAULT_CURRENCY_RATIO)))
            .andExpect(jsonPath("$.[*].currencyBase").value(hasItem(DEFAULT_CURRENCY_BASE.booleanValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultVirtualCurrencyShouldNotBeFound(String filter) throws Exception {
        restVirtualCurrencyMockMvc.perform(get("/api/virtual-currencies?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }


    @Test
    @Transactional
    public void getNonExistingVirtualCurrency() throws Exception {
        // Get the virtualCurrency
        restVirtualCurrencyMockMvc.perform(get("/api/virtual-currencies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVirtualCurrency() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);
        int databaseSizeBeforeUpdate = virtualCurrencyRepository.findAll().size();

        // Update the virtualCurrency
        VirtualCurrency updatedVirtualCurrency = virtualCurrencyRepository.findOne(virtualCurrency.getId());
        updatedVirtualCurrency
            .currencyIconUrl(UPDATED_CURRENCY_ICON_URL)
            .currencyCode(UPDATED_CURRENCY_CODE)
            .currencyName(UPDATED_CURRENCY_NAME)
            .initialDeposit(UPDATED_INITIAL_DEPOSIT)
            .rechargeRate(UPDATED_RECHARGE_RATE)
            .maxLimit(UPDATED_MAX_LIMIT)
            .currencyRatio(UPDATED_CURRENCY_RATIO)
            .currencyBase(UPDATED_CURRENCY_BASE);
        VirtualCurrencyDTO virtualCurrencyDTO = virtualCurrencyMapper.toDto(updatedVirtualCurrency);

        restVirtualCurrencyMockMvc.perform(put("/api/virtual-currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(virtualCurrencyDTO)))
            .andExpect(status().isOk());

        // Validate the VirtualCurrency in the database
        List<VirtualCurrency> virtualCurrencyList = virtualCurrencyRepository.findAll();
        assertThat(virtualCurrencyList).hasSize(databaseSizeBeforeUpdate);
        VirtualCurrency testVirtualCurrency = virtualCurrencyList.get(virtualCurrencyList.size() - 1);
        assertThat(testVirtualCurrency.getCurrencyIconUrl()).isEqualTo(UPDATED_CURRENCY_ICON_URL);
        assertThat(testVirtualCurrency.getCurrencyCode()).isEqualTo(UPDATED_CURRENCY_CODE);
        assertThat(testVirtualCurrency.getCurrencyName()).isEqualTo(UPDATED_CURRENCY_NAME);
        assertThat(testVirtualCurrency.getInitialDeposit()).isEqualTo(UPDATED_INITIAL_DEPOSIT);
        assertThat(testVirtualCurrency.getRechargeRate()).isEqualTo(UPDATED_RECHARGE_RATE);
        assertThat(testVirtualCurrency.getMaxLimit()).isEqualTo(UPDATED_MAX_LIMIT);
        assertThat(testVirtualCurrency.getCurrencyRatio()).isEqualTo(UPDATED_CURRENCY_RATIO);
        assertThat(testVirtualCurrency.isCurrencyBase()).isEqualTo(UPDATED_CURRENCY_BASE);
    }

    @Test
    @Transactional
    public void updateNonExistingVirtualCurrency() throws Exception {
        int databaseSizeBeforeUpdate = virtualCurrencyRepository.findAll().size();

        // Create the VirtualCurrency
        VirtualCurrencyDTO virtualCurrencyDTO = virtualCurrencyMapper.toDto(virtualCurrency);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restVirtualCurrencyMockMvc.perform(put("/api/virtual-currencies")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(virtualCurrencyDTO)))
            .andExpect(status().isCreated());

        // Validate the VirtualCurrency in the database
        List<VirtualCurrency> virtualCurrencyList = virtualCurrencyRepository.findAll();
        assertThat(virtualCurrencyList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteVirtualCurrency() throws Exception {
        // Initialize the database
        virtualCurrencyRepository.saveAndFlush(virtualCurrency);
        int databaseSizeBeforeDelete = virtualCurrencyRepository.findAll().size();

        // Get the virtualCurrency
        restVirtualCurrencyMockMvc.perform(delete("/api/virtual-currencies/{id}", virtualCurrency.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<VirtualCurrency> virtualCurrencyList = virtualCurrencyRepository.findAll();
        assertThat(virtualCurrencyList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VirtualCurrency.class);
        VirtualCurrency virtualCurrency1 = new VirtualCurrency();
        virtualCurrency1.setId(1L);
        VirtualCurrency virtualCurrency2 = new VirtualCurrency();
        virtualCurrency2.setId(virtualCurrency1.getId());
        assertThat(virtualCurrency1).isEqualTo(virtualCurrency2);
        virtualCurrency2.setId(2L);
        assertThat(virtualCurrency1).isNotEqualTo(virtualCurrency2);
        virtualCurrency1.setId(null);
        assertThat(virtualCurrency1).isNotEqualTo(virtualCurrency2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(VirtualCurrencyDTO.class);
        VirtualCurrencyDTO virtualCurrencyDTO1 = new VirtualCurrencyDTO();
        virtualCurrencyDTO1.setId(1L);
        VirtualCurrencyDTO virtualCurrencyDTO2 = new VirtualCurrencyDTO();
        assertThat(virtualCurrencyDTO1).isNotEqualTo(virtualCurrencyDTO2);
        virtualCurrencyDTO2.setId(virtualCurrencyDTO1.getId());
        assertThat(virtualCurrencyDTO1).isEqualTo(virtualCurrencyDTO2);
        virtualCurrencyDTO2.setId(2L);
        assertThat(virtualCurrencyDTO1).isNotEqualTo(virtualCurrencyDTO2);
        virtualCurrencyDTO1.setId(null);
        assertThat(virtualCurrencyDTO1).isNotEqualTo(virtualCurrencyDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(virtualCurrencyMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(virtualCurrencyMapper.fromId(null)).isNull();
    }
}
